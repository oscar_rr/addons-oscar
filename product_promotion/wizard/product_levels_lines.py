# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _


class ProductLevelsLines(models.TransientModel):
    _name = 'product.levels.lines'

    line_id = fields.Many2one(
        comodel_name='product.levels.window',
        string=_('Line ids')
    )

    name = fields.Char(
        readonly=True,
        string=_("Product"),
    )
    default_code = fields.Char(
        readonly=True,
        string=_("Code"),
    )
    # LEVEL 1
    level1_price = fields.Char(
        readonly=True,
        string=_("Level 1 Price"),
        store=True
    )
    level1_discount = fields.Char(
        readonly=True,
        string=_("Level 1 Discount"),
    )
    level1_promo = fields.Char(
        readonly=True,
        string=_("Level 1 Promo")
    )
    # LEVEL 2
    level2_price = fields.Char(
        readonly=True,
        string=_("Level 2 Price"),
    )
    level2_discount = fields.Char(
        readonly=True,
        string=_("Level 2 Discount"),
    )
    level2_promo = fields.Char(
        readonly=True,
        string=_("Level 2 Promo")
    )
    # LEVEL 3
    level3_price = fields.Char(
        readonly=True,
        string=_("Level 3 Price"),
    )
    level3_discount = fields.Char(
        readonly=True,
        string=_("Level 3 Discount"),
    )
    level3_promo = fields.Char(
        readonly=True,
        string=_("Level 3 Promo")
    )
    # LEVEL 4
    level4_price = fields.Char(
        readonly=True,
        string=_("Level 4 Price"),
    )
    level4_discount = fields.Char(
        readonly=True,
        string=_("Level 4 Discount"),
    )
    level4_promo = fields.Char(
        readonly=True,
        string=_("Level 4 Promo")
    )
    # LEVEL 5
    level5_price = fields.Char(
        readonly=True,
        string=_("Level 5 Price"),
    )
    level5_discount = fields.Char(
        readonly=True,
        string=_("Level 5 Discount"),
    )
    level5_promo = fields.Char(
        readonly=True,
        string=_("Level 5 Promo")
    )
    # LEVEL 6
    level6_price = fields.Char(
        readonly=True,
        string=_("Level 6 Price"),
    )
    level6_discount = fields.Char(
        readonly=True,
        string=_("Level 6 Discount"),
    )
    level6_promo = fields.Char(
        readonly=True,
        string=_("Level 6 Promo")
    )
    # LEVEL 7
    level7_price = fields.Char(
        readonly=True,
        string=_("Level 7 Price"),
    )
    level7_discount = fields.Char(
        readonly=True,
        string=_("Level 7 Discount"),
    )
    level7_promo = fields.Char(
        readonly=True,
        string=_("Level 7 Promo")
    )
    # LEVEL 8
    level8_price = fields.Char(
        readonly=True,
        string=_("Level 8 Price"),
    )
    level8_discount = fields.Char(
        readonly=True,
        string=_("Level 8 Discount"),
    )
    level8_promo = fields.Char(
        readonly=True,
        string=_("Level 8 Promo")
    )
    # LEVEL 9
    level9_price = fields.Char(
        readonly=True,
        string=_("Level 9 Price"),
    )
    level9_discount = fields.Char(
        readonly=True,
        string=_("Level 9 Discount"),
    )
    level9_promo = fields.Char(
        readonly=True,
        string=_("Level 9 Promo")
    )
    # LEVEL 10
    level10_price = fields.Char(
        readonly=True,
        string=_("Level 10 Price"),
    )
    level10_discount = fields.Char(
        readonly=True,
        string=_("Level 10 Discount"),
    )
    level10_promo = fields.Char(
        readonly=True,
        string=_("Level 10 Promo")
    )
