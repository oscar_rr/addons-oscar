# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
_logger = logging.getLogger(__name__)


class RepeatedProducts(models.TransientModel):
    _name = 'repeated.products.window'
    _rec_name = 'promotion_id'

    promotion_id = fields.Many2one(
        comodel_name='product.promotion',
        string=_('Promotion'),
    )

    repeated_line_ids = fields.One2many(
        comodel_name='repeated.products.lines',
        inverse_name='line_id',
        string=_('Lines'),
    )
    aproved_denied_line_ids = fields.One2many(
        comodel_name='aproved.denied.products.lines',
        inverse_name='line_id',
        string=_('Lines'),
        readonly=True,
    )
    cond1 = fields.Boolean('cond1', compute='_compute_conditions')
    cond2 = fields.Boolean('cond2', compute='_compute_conditions')

    @api.multi
    def show_wizard_repeated_products(
            self, repeated_products, aproved_products,
            denied_products, average_cost_d, discount_d,
            list_price_d, discount_price_d,
            list_price_converted_d, discount_price_converted_d,
            average_cost_a, discount_a,
            list_price_a, discount_price_a, list_price_converted_a,
            discount_price_converted_a):
        wizard_form = self.env.ref(
            'product_promotion.repeated_products_window',
            False
        )
        prom_object = self.env['product.promotion']
        prod_object = self.env['product.template']
        prom_line_object = self.env['product.promotion.lines']
        repeated_values = []
        aproved_denied_values = []

        # This is for repeated products only
        if repeated_products:
            promotions = prom_object.search([
                ('state', 'in', ['draft', 'active'])
            ])
            repeated_lines = prom_line_object.search([
                    ('promotion_id', 'in', promotions.ids),
                    ('product_id', 'in', repeated_products)
                ])

            for record in repeated_lines:
                repeated_values.append((0, 0, {
                    'name': record.product_id.name,
                    'product_reference': record.product_id.default_code,
                    'promotion_name': record.promotion_id.name
                }))

        if aproved_products:
            aproved_lines = prod_object.search([
                    ('id', 'in', aproved_products)
                ])

            for record in aproved_lines:
                x = list(aproved_lines).index(record)
                aproved_denied_values.append((0, 0, {
                    'name': record.name,
                    'product_reference': record.default_code,
                    'avg_cost': average_cost_a[x],
                    'discount': discount_a[x],
                    'price_list': list_price_a[x],
                    'disc_price': discount_price_a[x],
                    'price_listc': list_price_converted_a[x],
                    'disc_pricec': discount_price_converted_a[x],
                }))

        # This is for denied products only
        if denied_products:
            denied_lines = prod_object.search([
                    ('id', 'in', denied_products)
                ])

            for record in denied_lines:
                x = list(denied_lines).index(record)
                aproved_denied_values.append((0, 0, {
                    'name': record.name,
                    'product_reference': record.default_code,
                    'avg_cost': average_cost_d[x],
                    'discount': discount_d[x],
                    'price_list': list_price_d[x],
                    'disc_price': discount_price_d[x],
                    'price_listc': list_price_converted_d[x],
                    'disc_pricec': discount_price_converted_d[x],
                    'status': True,
                }))

        view_id = self.env['repeated.products.window']
        vals = {
            'repeated_line_ids': repeated_values,
            'aproved_denied_line_ids': aproved_denied_values,
        }
        new = view_id.create(vals)
        return {
            'name': 'Products warning',
            'type': 'ir.actions.act_window',
            'res_model': 'repeated.products.window',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    @api.one
    def _compute_conditions(self):
        self.cond1 = len(self.repeated_line_ids) > 0
        self.cond2 = len(self.aproved_denied_line_ids) > 0
