# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, _
from openerp import tools
from olib.oCsv.oCsv import OCsv
from datetime import datetime as dt
from dateutil import tz
import logging
_logger = logging.getLogger(__name__)


class PromotionReportWizard(models.TransientModel):
    _name = 'promotion.report.wizard'

    name = fields.Char(
        string=_("Wizard")
    )

    promotion_id = fields.Many2one(
        comodel_name='product.promotion',
        string=_("Promotion ID")
    )

    report_data_id = fields.Many2one(
        comodel_name='promotion.report.data',
        string=_("Promotion Data ID"),
    )

    start_date = fields.Datetime(
        string=_("Start Date"),
        required=True,
    )
    end_date = fields.Datetime(
        string="End Date",
        required=True,
    )
    start_product = fields.Many2one(
        comodel_name='product.template',
        string=_("Start Product"),
    )
    end_product = fields.Many2one(
        comodel_name='product.template',
        string=_("End Product"),
    )
    branch_office = fields.Many2many(
        comodel_name='account.cost.center',
        string=_("Branch office"),
    )

    state = fields.Selection([
        ('active', 'Active'),
        ('expire', 'Expired'),
        ('cancel', 'Canceled')
    ], string='State')

    def apply_filters(self, columns=''):
        sql = """
            SELECT {0} FROM product_promotion_lines AS pl
            INNER JOIN product_promotion AS pp
            ON pl.promotion_id = pp.id
            WHERE pp.start_date >= '{1}'
            AND pp.end_date <= '{2}'
            AND state LIKE '%{3}%'
        """.format(columns, self.start_date, self.end_date, self.state or '')
        if self.start_product and not self.end_product:
            sql += " AND product_code = '%s'" % self.start_product.default_code
        elif self.start_product and self.end_product:
            sql += " AND product_code >= '%s' AND product_code <= '%s' " \
                % (
                    self.start_product.default_code,
                    self.end_product.default_code
                )

        if self.branch_office.ids:
            ids = str(self.branch_office.ids).replace('[', '').replace(']', '')
            sql += """
                    AND pl.id IN (SELECT product_promotion_lines_id FROM
                    account_cost_center_product_promotion_lines_rel WHERE
                    product_promotion_lines_id = pl.id
                    AND account_cost_center_id IN (%s))
                """ % (ids)

        return sql

    @api.multi
    def screen_report(self):
        tree_form = self.env.ref(
            'product_promotion.promotion_report_data',
            False
        )
        sql = self.apply_filters(columns='pl.id')
        self.env.cr.execute(sql)

        promotion_ids = self.env.cr.fetchall()

        return {
            'name': 'Promotion Report Data',
            'type': 'ir.actions.act_window',
            'res_model': 'product.promotion.lines',
            'view_id': tree_form.id,
            'view_type': 'tree',
            'view_mode': 'tree',
            'target': 'current',
            'domain': [('id', 'in', promotion_ids)],
        }

    @api.multi
    def file_report(self):
        id = self.to_csv_by_ids()

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id.id),
            'target': 'self',
        }

    def to_csv_by_ids(self):
        array = []
        sql = self.apply_filters(columns='pl.id')
        self.env.cr.execute(sql)

        dataset = self.env.cr.fetchall()
        for record in dataset:
            array.append(record[0])
        print(array)
        dataset = self.env['product.promotion.lines'].browse(array)
        print(dataset)
        dataset_csv = [
            [
                _('Product Code'),
                _('Product'),
                _('Promotion name'),
                _('Folio'),
                _('Status'),
                _('Creation Date'),
                _('Start Date'),
                _('End Date'),
                _('User'),
                _('Global discount'),
                _('Price'),
                _('Discount'),
                _('Discount price'),
                _('Is level'),
                _('Lvl 1 Price'),
                _('Lvl 1 Discount'),
                _('Lvl 1 Promo'),
                _('Lvl 2 Price'),
                _('Lvl 2 Discount'),
                _('Lvl 2 Promo'),
                _('Lvl 3 Price'),
                _('Lvl 3 Discount'),
                _('Lvl 3 Promo'),
                _('Lvl 4 Price'),
                _('Lvl 4 Discount'),
                _('Lvl 4 Promo'),
                _('Lvl 5 Price'),
                _('Lvl 5 Discount'),
                _('Lvl 5 Promo'),
                _('Lvl 6 Price'),
                _('Lvl 6 Discount'),
                _('Lvl 6 Promo'),
                _('Lvl 7 Price'),
                _('Lvl 7 Discount'),
                _('Lvl 7 Promo'),
                _('Lvl 8 Price'),
                _('Lvl 8 Discount'),
                _('Lvl 8 Promo'),
                _('Lvl 9 Price'),
                _('Lvl 9 Discount'),
                _('Lvl 9 Promo'),
                _('Lvl 10 Price'),
                _('Lvl 10 Discount'),
                _('Lvl 10 Promo'),
                _('Cur'),
                _('Branch office'),
                _('Segment'),
                _('Cancel date'),
                _('Cancel user'),
            ]
        ]

        for row in dataset:
            dataset_csv.append([
                unicode(self.set_default(row.product_code)).encode('utf8'),
                unicode(self.set_default(row.product_id.name)).encode('utf8'),
                unicode(self.set_default(row.promotion_name)).encode('utf8'),
                unicode(self.set_default(row.promotion_folio)).encode('utf8'),
                unicode(self.set_default(row.promotion_status)).encode('utf8'),
                unicode(self.utc_mexico_city(row.creation_date)).
                encode('utf8'),
                unicode(self.utc_mexico_city(row.start_date)).encode('utf8'),
                unicode(self.utc_mexico_city(row.end_date)).encode('utf8'),
                unicode(self.set_default(row.user.name)).encode('utf8'),
                unicode(self.set_default(row.global_discount)).encode('utf8'),
                unicode(self.set_default(row.price)).encode('utf8'),
                unicode(self.set_default(row.disc)).encode('utf8'),
                unicode(self.set_default(row.disc_price)).encode('utf8'),
                unicode(self.set_default(row.is_level)).encode('utf8'),
                unicode(self.set_default(row.level1_price)).encode('utf8'),
                unicode(self.set_default(row.level1_discount)).encode('utf8'),
                unicode(self.set_default(row.level1_promo)).encode('utf8'),
                unicode(self.set_default(row.level2_price)).encode('utf8'),
                unicode(self.set_default(row.level2_discount)).encode('utf8'),
                unicode(self.set_default(row.level2_promo)).encode('utf8'),
                unicode(self.set_default(row.level3_price)).encode('utf8'),
                unicode(self.set_default(row.level3_discount)).encode('utf8'),
                unicode(self.set_default(row.level3_promo)).encode('utf8'),
                unicode(self.set_default(row.level4_price)).encode('utf8'),
                unicode(self.set_default(row.level4_discount)).encode('utf8'),
                unicode(self.set_default(row.level4_promo)).encode('utf8'),
                unicode(self.set_default(row.level5_price)).encode('utf8'),
                unicode(self.set_default(row.level5_discount)).encode('utf8'),
                unicode(self.set_default(row.level5_promo)).encode('utf8'),
                unicode(self.set_default(row.level6_price)).encode('utf8'),
                unicode(self.set_default(row.level6_discount)).encode('utf8'),
                unicode(self.set_default(row.level6_promo)).encode('utf8'),
                unicode(self.set_default(row.level7_price)).encode('utf8'),
                unicode(self.set_default(row.level7_discount)).encode('utf8'),
                unicode(self.set_default(row.level7_promo)).encode('utf8'),
                unicode(self.set_default(row.level8_price)).encode('utf8'),
                unicode(self.set_default(row.level8_discount)).encode('utf8'),
                unicode(self.set_default(row.level8_promo)).encode('utf8'),
                unicode(self.set_default(row.level9_price)).encode('utf8'),
                unicode(self.set_default(row.level9_discount)).encode('utf8'),
                unicode(self.set_default(row.level9_promo)).encode('utf8'),
                unicode(self.set_default(row.level10_price)).encode('utf8'),
                unicode(self.set_default(row.level10_discount)).encode('utf8'),
                unicode(self.set_default(row.level10_promo)).encode('utf8'),
                unicode(self.set_default(row.cur)).encode('utf8'),
                unicode(self.set_default(row.branch_off)).encode('utf8'),
                unicode(self.set_default(row.segmnt)).encode('utf8'),
                unicode(self.utc_mexico_city(row.cancel_date)).encode('utf8'),
                unicode(self.set_default(row.cancel_user.name)).encode('utf8'),
            ])
        # dataset_csv.append(['', ''])
        obj_ocsv = OCsv()
        file = obj_ocsv.csv_base64(dataset_csv)
        return self.env['r.download'].create(vals={
            'file_name': 'Promotions_Report.csv',
            'type_file': 'csv',
            'file': file,
        })

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default

    def utc_mexico_city(self, date):
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('America/Mexico_City')
            utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''
