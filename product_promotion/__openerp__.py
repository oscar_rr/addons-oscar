# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Product Promotion',
    'version': '0.1',
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'category': 'Products',
    'description': """
        This module allows the user to add/remove promotions to the products
         """,
    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': [
        'base',
        'mail',
        'price_list',
        'centro_de_costos',
        'sale',
        'sale_stock',
        'postal_code',
        'cotizaciones',
        'base_action_rule',
        'reports'
    ],
    'data': [
        'views/product_promotion_view.xml',
        'wizard/repeated_products.xml',
        'wizard/product_levels.xml',
        'data/ir_sequence_data.xml',
        'data/activate_action.xml',
        'data/expire_action.xml',
        'wizard/report_wizard.xml',
        'security/ir.model.access.csv',
        'views/res_config_view.xml',
    ],
    'installable': True,
    'active': False,
}
