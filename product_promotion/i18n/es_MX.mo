��    �      �          p  �   q  T  9  �  �  �  \      �    �   �  R   -     �     �  .   �     �     �     �     �          #     0     ?     M     [     a     h     o     {     �     �     �     �     �     �     �  
   �  
   �               #  .   '     V     i     x     �     �  
   �     �     �  �   �  +   V  b   �  ^   �  8   D      }      �   4   �      �      �      �   $   �      !     "!     '!  	   -!     7!     G!  �   W!     �!  /   �!  V   "     g"     u"     ~"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     $#     2#     @#     Q#     _#     m#     ~#     �#     �#     �#     �#     �#     �#     �#     �#     $     $     !$     2$     @$     N$     _$     m$  2   {$     �$     �$     �$     �$     �$     �$     �$     �$     %     %      %     -%     :%     I%     U%     a%     p%     |%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     &     &     $&     3&     ?&     K&     Z&     f&     r&     �&  "   �&     �&     �&  
   �&     �&     �&     �&     '     '     )'     @'     Y'     t'     �'     �'     �'  	   �'     �'     �'     �'     �'     (     (     1(     H(     a(     p(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(     �(     )     )     !)  
   ()     3)     A)     G)     N)  0   V)  2   �)  5   �)     �)  l   	*     v*     �*  
   �*     �*     �*     �*  0   �*  .   �*  "   +  -   8+     f+     i+  !   l+  E  �+  �   �,  X  �-  �  �/  �  �1    [3  �  v5  �   57  J   �7     8     8  2   8     Q8     n8     |8  &   �8     �8     �8     �8     �8     �8     �8     �8     9     	9     9     49     H9  	   ]9     g9     n9      v9      �9  
   �9  	   �9     �9     �9     �9  2   �9     -:     H:     W:     _:  	   s:     }:     �:     �:  g   �:  /   ;  g   D;  g   �;  D   <     Y<     n<  2   �<     �<  	   �<     �<  0   �<     =     =     #=  
   )=     4=     E=  �   V=     �=  /   �=  Z   >     j>     y>     �>     �>     �>     �>     �>     �>     �>     ?     ?     )?     8?     J?     Y?     g?     y?     �?     �?     �?     �?     �?     �?     �?     �?     @     @     #@     5@     D@     R@     d@     s@     �@     �@     �@  <   �@     �@     �@     
A     A     #A     ,A     4A     FA     UA     cA     vA     �A     �A     �A     �A     �A     �A     �A     �A     B     B     "B     4B     CB     QB     dB     sB     �B     �B     �B     �B     �B     �B     �B     �B      C     C     "C  %   +C  (   QC     zC     �C     �C     �C     �C     �C     �C     �C  !   D  %   5D     [D     tD     �D     �D  
   �D     �D     �D     �D     E     E     #E     9E  !   PE     rE     �E     �E  	   �E  	   �E     �E     �E     �E     F     F     !F     6F     ?F     HF     ZF     nF     uF     �F     �F     �F     �F  .   �F  9   �F  ;   G     NG  o   lG     �G     �G     �G     H     H     "H  <   %H  <   bH  ,   �H  ,   �H     �H     �H  !   �H     �   �       �   `   �       �   z       ]   H   X          �   �       �   �   a   >   y      &   *       �              �           �   /       L       :   s   �      �       �   l   b          +   o   �   -           Y   A   �   j   P           6           t   w       �          E   m   �   �       |   �   ^   �   0       x   �   #      W   �   %   �           7      �      ;       @   
   R           �              �   �   �      �   �   Z   (   �   �   �   �   ~           1   i      �   C   �   �   e   �           �       B   �   �   M   $   �       �          �   �          h   J   �   ?       �       n       T   �   c       <   �       k   _   4              [       U   !   �          g       �   �   )   S   }   "   �   �       �   �          �   �         �      �   �   =   r   �          F   v       ,      G   \   �   �   �   �           9   �       5      �   �   �   �   8       3       Q                  .      {              f   �   q   	   u   �   V   I   O   N   �       �   p              �   �      D       d   �   �       2      '           K    
                                            Discount can´t be lower or
                                            equal than zero

                                            in discount level #%s 
                                    '%s'

                                    Discount price is lower than the average
                                    cost of the product

                                    Average cost: '%.2f %s'

                                    Discount: '%.2f%s'

                                    List price: '%.2f %s'

                                    Discount price: '%.2f %s'

                                    List price(converted): '%.2f %s'

                                    Discount price(converted): '%.2f %s'

                                     
                                    '%s'

                                    Discount price is lower than the average
                                    cost of the product

                                    Average cost: '%.2f %s'

                                    Discount: '%.2f%s', Level: '%s'

                                    Price: '%.2f %s',

                                    Discount price: '%.2f %s'

                                     
                                '%s'

                                Discount price is lower than the average
                                cost of the product

                                Average cost: '%.2f %s'

                                Discount: '%.2f%s'

                                List price: '%.2f %s'

                                Discount price: '%.2f %s'

                                 
                            '%s'

                            Discount price is lower than the average
                            cost of the product

                            Average cost: '%.2f %s'

                            Discount: '%.2f%s', Level '%s'

                            List price: '%.2f %s'

                            Discount price: '%.2f %s'

                            List price(converted): '%.2f %s'

                            Discount price(converted): '%.2f %s'

                             
                            '%s'

                            Discount price is lower than the average
                            cost of the product

                            Average cost: '%.2f %s'

                            Discount: '%.2f%s', Level: '%s'

                            Price: '%.2f %s',

                            Discount price: '%.2f %s'

                             
                        The following product '%s' is already set in the
                        promotion named '%s'

                     
                    Product(s) not found in your search criteria
                 Activate promotion Active Add products to the promotion when it's active Allows the maximum discount Applies on WEB Apply Apply discount level prices Apply levels Average cost Basic settings Branch Office Branch office Brand Browse Cancel Cancel User Cancel date Cancel promotion Cancel user Canceled Close Code Configure Product Promotion Configure Product Promotions Created by Created on Creation Date Creation date Cur Date of the last message posted on the record. Delete all records Delete filters Deleted Deleted product %s Discount Discount % Discount (%) Discount Price Discount can´t be lower
                                        or equal than zero

                                        in discount level #%s Discount can´t be lower or equal than zero Discount can´t be lower or equal than zero

                                in discount level #%s Discount can´t be lower or equal than zero

                            in discount level #%s Discount levels can't be lower
                than zero Discount price Discount price converted Display and manage the avaliable product promotions. Draft End Date End Product End date is lower than Start date!!! Expired File Folio Followers Global Discount Global discount Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. If this is active, then its posible to add products to an
            active promotion Is a Follower Is level Last Message Date Last Updated by Last Updated on Level Level 1 Discount Level 1 Price Level 1 Promo Level 10 Discount Level 10 Price Level 10 Promo Level 2 Discount Level 2 Price Level 2 Promo Level 3 Discount Level 3 Price Level 3 Promo Level 4 Discount Level 4 Price Level 4 Promo Level 5 Discount Level 5 Price Level 5 Promo Level 6 Discount Level 6 Price Level 6 Promo Level 7 Discount Level 7 Price Level 7 Promo Level 8 Discount Level 8 Price Level 8 Promo Level 9 Discount Level 9 Price Level 9 Promo Level Discounts can´t be lower or equal than zero Levels Levels promotion Limit discount Line Line ids Lines Lvl 1 Discount Lvl 1 Price Lvl 1 Promo Lvl 10 Discount Lvl 10 Price Lvl 10 Promo Lvl 2 Discount Lvl 2 Price Lvl 2 Promo Lvl 3 Discount Lvl 3 Price Lvl 3 Promo Lvl 4 Discount Lvl 4 Price Lvl 4 Promo Lvl 5 Discount Lvl 5 Price Lvl 5 Promo Lvl 6 Discount Lvl 6 Price Lvl 6 Promo Lvl 7 Discount Lvl 7 Price Lvl 7 Promo Lvl 8 Discount Lvl 8 Price Lvl 8 Promo Lvl 9 Discount Lvl 9 Price Lvl 9 Promo Market segment Messages Messages and communication history Please set up an end date first Price Price List Price list converted Product Product Code Product Levels Product Promotion Product Promotion Form Product Promotion Report Product Promotion Settings Product Warning! Product code Product name Product reference Promotion Promotion Activate Action Promotion Data ID Promotion Expire Action Promotion ID Promotion Name Promotion Report Data Promotion add products Promotion limit discount Promotion name Promotion setup Promotions Report RC Cur Rep Cost Repeated products Report Wizard Sales Order Sales Order Line Screen Search filters Section Segment Select Dates Select Product Serial Start Date Start Product State Status Summary The current end date is behind today's date/hour The current start date is behind today's date/hour The following products are already set in a promotion The limit discount is %s The products found in red can't be added because their applied discount price is lower than the average cost Unread Messages User Warning!!! Web promotion alias Wizard Yes You can't set a past date from today's date/hour You can't set a past date from tomorrow's date You must set a start date first!!! and cannot be added to the current promotion. or to {'currency_id' : parent.currency} Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-30 17:46+0000
PO-Revision-Date: 2018-08-30 13:02-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 
                                            El descuento no puede ser menor o
                                            igual que cero

                                            en el descuento nivel #%s 
                                    '%s'

                                    El precio con descuento es más bajo que el costo promedio del producto

                                    Costo promedio: '%.2f %s'

                                    Descuento: '%.2f%s'

                                    Precio de lista: '%.2f %s'

                                    Precio con descuento: '%.2f %s'

                                    Precio de lista (Convertido): '%.2f %s'

                                    Precio con descuento (Convertido): '%.2f %s'

                                     
                                    '%s'

                                    El precio con descuento es más bajo que el costo promedio del producto

                                    Costo promedio: '%.2f %s'

                                    Descuento: '%.2f%s', Nivel: '%s'

                                    Precio: '%.2f %s',

                                    Precio con descuento: '%.2f %s'

                                     
                                '%s'

                                El precio con descuento es más bajo que el costo promedio del producto

                                Costo promedio: '%.2f %s'

                                Descuento: '%.2f%s'

                                Precio de lista: '%.2f %s'

                                Precio con descuento: '%.2f %s'

                                 
                            '%s'

                            El precio con descuento es más bajo que el costo promedio del producto

                            Costo promedio: '%.2f %s'

                            Descuento: '%.2f%s', Nivel '%s'

                            Precio de lista: '%.2f %s'

                            Precio con descuento: '%.2f %s'

                            Precio de lista(Convertido): '%.2f %s'

                            Precio con descuento(Convertido): '%.2f %s'

                             
                                    '%s'

                                    El precio con descuento es más bajo que el costo promedio del producto

                                    Costo promedio: '%.2f %s'

                                    Descuento: '%.2f%s', Nivel: '%s'

                                    Precio: '%.2f %s',

                                    Precio con descuento: '%.2f %s'

                                     
                       El producto '%s' ya se encuentra en la
                        promocion llamada '%s'

                     
                    No se encontró nada en tu búsqueda
                 Activar Promoción Activo Agregar productos cuando la promoción este activa Permite un máximo descuento Aplica en WEB Aplicar Aplicar precios de nivel con descuento Aplicar Niveles Costo Promedio Configuraciones básicas Sucursal Sucursal Marca Buscar Cancel Usuario que canceló Fecha de cancelación Cancelar Promoción Usuario que canceló Cancelado Cerrar Código Configure Promoción de Producto Configure Promoción de producto Creado por Creado el Fecha de creación Fecha de creación Moneda Fecha del último mensaje publicado en el registro Borrar todos los registros Borrar filtros Borrado Producto borrado %s Descuento Descuento % Descuento (%) Precio con descuento El descuento no puede ser menor o igual que cero

                            en el descuento nivel #%s El descuento no puede ser menor o igual a cero. El descuento no puede ser menor o igual que cero

                            en el descuento nivel #%s El descuento no puede ser menor o igual que cero

                            en el descuento nivel #%s Los descuentos de nivel no pueden ser menor
                que cero Precio con descuento Precio con descuento convertido Muestra y administra las promociones de productos. Borrador Fecha fin Producto fin La fecha fin es más baja que la fecha inicio!!! Expirado Archivo Folio Seguidores Descuento Global Descuento global Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. Si está activo, entonces es posible agregar productos a una
            promoción activa Es un seguidor Es Nivel Fecha del último mensaje Última actualicación por Última actualización en Nivel Descuento Nivel 1 Precio Nivel 1 Promo Nivel 1 Descuento Nivel 10 Precio Nivel 10 Promo Nivel 10 Descuento Nivel 2 Precio Nivel 2 Promo Nivel 2 Descuento Nivel 2 Precio Nivel 3 Promo Nivel 3 Descuento Nivel 4 Precio Nivel 4 Promo Nivel 4 Descuento Nivel 5 Precio Nivel 5 Promo Nivel 5 Descuento Nivel 6 Precio Nivel 6 Promo Nivel 6 Descuento Nivel 7 Precio Nivel 7 Promo Nivel 7 Descuento Nivel 8 Precio Nivel 8 Promo Nivel 8 Descuento Nivel 9 Precio Nivel 9 Promo Nivel 9 Los descuentos de nivel no pueden ser menor o igual que cero Niveles Promocion de niveles Descuento límite Línea Line ids Líneas Descuento Nivel 1 Precio Nivel 1 Promo Nivel 1 Descuento Nivel 10 Precio Nivel 10 Promo Nivel 10 Descuento Nivel 2 Precio Nivel 2 Promo Nivel 2 Descuento Nivel 3 Precio Nivel 3 Promo Nivel 3 Descuento Nivel 4 Precio Nivel 4 Promo Nivel 4 Descuento Nivel 5 Precio Nivel 5 Promo Nivel 5 Descuento Nivel  6 Precio Nivel 6 Promo Nivel 6 Descuento Nivel 7 Precio Nivel 7 Promo Nivel 7 Descuento Nivel 8 Precio Nivel 8 Promo Nivel 8 Descuento Nivel 9 Precio Nivel 9 Promo Nivel 9 Segmento de mercado Mensajes Historia de communicación y mensajes Porfavor configure una fecha fin primero Precio Precio de lista Precio de lista convertido Producto Código de producto Niveles de producto Promocion de Producto Forma de promoción de producto Reporte de Promoción de Producto Configuración Promocion de Productos Advertencia de Producto! Código de producto Nombre de producto Referencia de producto Promoción Promotion Activate Action Promotion Data ID Promotion Expire Action Promotion ID Nombre de Promoción Promotion Report Data Promotion add products Limite de descuento de Promoción Nombre de promoción Configuración de Promoción Reporte de promoción Moneda CR Costo Rep Productos repetidos Report Wizard Pedidos de Venta Línea pedido de venta Pantalla Filtros de búsqueda Sección Segmento Selecciona Fechas Selecciona Producto Serial Fecha Inicio Producto Inicial Estado Status Summary La fecha fin es menor que la fecha/hora de hoy La fecha seleccionada esta atrás de la fecha/hora actual Los siguientes productos ya se encuentran en una promoción El límite de descuento es %s Los productos en rojo no pueden ser agregados porque su precio con descuento es más bajo que el costo promedio Unread Messages Usuario Advertencia!!! Alias de promoción Web Wizard Si No puedes configurar una fecha pasada a la fecha/hora de hoy No puedes configurar una fecha pasada a la fecha de mañana. Debes configurar una fecha inicio primero!!! y no puede agregarse a la promoción actual. o a {'currency_id' : parent.currency} 