# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
_logger = logging.getLogger(__name__)


class ProductPromotionLevelLines(models.Model):
    _name = 'product.promotion.level.lines'
    level_line_id = fields.Many2one(
        'price.level',
        'Level',
        required=True,
        readonly=True,
    )
    promotion_id = fields.Many2one('product.promotion', _('Promotion'))
    discount = fields.Float(_('Discount %'), digits=(5, 2))
