# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, exceptions, _
import datetime
from datetime import timedelta
import logging
_logger = logging.getLogger(__name__)


class ProductPromotion(models.Model):
    _inherit = ['mail.thread']
    _name = 'product.promotion'
    _rec_name = 'folio'
    _description = _('Product Promotion')

    # We validate the user's center costs assigned that he only can assign
    @api.model
    def center_cost_domain(self):
        res = []
        res = self.env.user.centro_costo_id.ids
        return [('id', 'in', res)]

    def _get_levels(self):
        ids = self.env['price.level'].search([], order='level_id asc').ids
        level_lines = self.env['product.promotion.level.lines']
        values = []
        for _id in ids:
            values.append({
                'level_line_id': _id,
                'discount': 0,
            })
        return values

    name = fields.Char(
        string=_("Promotion"),
        required=True,
    )

    level_ids = fields.One2many(
        'product.promotion.level.lines',
        'promotion_id',
        _('Levels'),
        default=_get_levels
    )

    section = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Sección')]",
        string=_("Section"),
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        domain=[('types.type', '=', 'Marca')],
        string=_("Brand"),
    )
    line = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Línea')]",
        string=_("Line"),
    )
    serial = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Serie')]",
        string=_("Serial"),
    )
    discount = fields.Float(
        string=_("Discount"),
        required=False,
    )

    start_date = fields.Datetime(
        required=True,
        string=_("Start Date"),
    )

    end_date = fields.Datetime(
        required=True,
        string=_("End Date"),
    )

    show_web = fields.Boolean(
        string=_("Applies on WEB"),
    )

    web_alias = fields.Char(
        string=_("Web promotion alias"),
    )
    branch_office = fields.Many2many(
        comodel_name='account.cost.center',
        domain=center_cost_domain,
        string=_("Branch office"),
    )
    segment = fields.Many2many(
        comodel_name='product.segment',
        string=_("Market segment"),
    )
    line_ids = fields.One2many(
        comodel_name='product.promotion.lines',
        inverse_name='promotion_id',
        string=_("Lines"),
    )

    folio = fields.Char(
        string=_('Folio'),
        states={'draft': [('readonly', False)]},
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        size=50,
        translate=True,
        copy=False,
    )

    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('expire', 'Expired'),
            ('cancel', 'Canceled'),
            ], default='draft',
            index=True,
            track_visibility='always')

    state_active_button = fields.Selection([
            ('on', 'On'),
            ('off', 'Off')
            ], default='off',
            index=True,
            compute='invisible_active_button')

    state_cancel_button = fields.Selection([
            ('on', 'On'),
            ('off', 'Off')
            ], default='off',
            index=True,
            compute='invisible_cancel_button')

    apply_levels = fields.Boolean(_('Apply levels'))
    is_level = fields.Char(_('Levels promotion'), store=True, default="No")
    cancel_user = fields.Many2one(
        comodel_name='res.users',
        string=_("Cancel User"),
    )
    cancel_date = fields.Datetime(
        string=_("Cancel date"),
    )

    @api.multi
    def expire_action(self):
        if self.state == 'active':
            date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            for record in self:
                if date_now > record.end_date:
                    record.state = 'expire'
                else:
                    record.state = 'draft'

    @api.multi
    def activate_action(self):
        if self.state == 'draft':
            for record in self:
                record.state = 'active'

    @api.onchange('start_date')
    def _onchange_start_date(self):
        date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if self.start_date is False:
            return {}
        elif self.start_date < date_now:
            raise exceptions.Warning(
                _("You can't set a past date from today's date/hour"))
        else:
            return{}

    @api.onchange('end_date')
    def _onchange_end_date(self):
        date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # ************* Sum 1 day to self.end_date **************+******
        # if self.end_date is not False:
        #     datetime_date = \
        #     datetime.datetime.strptime(self.end_date, "%Y-%m-%d %H:%M:%S")
        #     days_sum = datetime_date + datetime.timedelta(days=1)
        #     string_date =\
        #     datetime.datetime.strftime(days_sum, "%Y-%m-%d %H:%M:%S")

        if self.start_date:
            if self.end_date < self.start_date:
                raise exceptions.Warning(
                    _("End date is lower than Start date!!!"))

        if self.start_date is False and self.end_date:
            raise exceptions.Warning(_("You must set a start date first!!!"))

        if self.end_date is False:
            return {}

        if self.end_date < date_now:
            raise exceptions.Warning(
                _("You can't set a past date from tomorrow's date"))
        else:
            return{}

    # Button to calculate discount prices by levels
    @api.multi
    def discount_level_prices(self):
        values = []
        price_values = []
        calc_values = []
        promo_values = []

        line_ids = self.line_ids
        apply_levels = self.apply_levels
        level_ids = self.level_ids

        currency_object = self.env['res.currency']
        currency_rate = currency_object.search([('name', '=', 'USD')])
        company_object = self.env['res.company']
        company_currency = company_object.search([
            ('id', '=', '1')]).currency_id.name
        percentage_symbol = '%'
        for x in range(0, len(line_ids)):
            product_currency = \
                self.line_ids[x].product_id.list_price_currency.name
            average_cost = self.line_ids[x].product_id.standard_price
            average_cost_currency =\
                self.line_ids[x].product_id.average_cost_currency_ro.name
            list_price_currency =\
                self.line_ids[x].product_id.list_price_currency.name
            list_price = self.line_ids[x].product_id.product_price_levels_nopop
            discount_lines = self.line_ids[x].promotion_id.level_ids
            discount_list_price_calc = list_price[x].price *\
                discount_lines[x].discount / 100
            discount_list_price =\
                list_price[x].price - discount_list_price_calc
            rounding = currency_rate.rate_sale_silent

            if product_currency != company_currency:
                if product_currency == 'USD':
                    conversion = list_price[x].price / rounding
                    calc = discount_lines[x].discount * conversion / 100
                    price_discount = conversion - calc
                    if price_discount < average_cost:
                        self.product_id = False
                        raise exceptions.Warning(_("""
                            '%s'\n
                            Discount price is lower than the average
                            cost of the product\n
                            Average cost: '%.2f %s'\n
                            Discount: '%.2f%s', Level '%s'\n
                            List price: '%.2f %s'\n
                            Discount price: '%.2f %s'\n
                            List price(converted): '%.2f %s'\n
                            Discount price(converted): '%.2f %s'\n
                            """ % (self.line_ids[x].product_id.name,
                                   average_cost, average_cost_currency,
                                   discount_lines[x].discount,
                                   percentage_symbol,
                                   discount_lines[x].level_line_id.id,
                                   list_price[x].price, list_price_currency,
                                   discount_list_price, list_price_currency,
                                   conversion, average_cost_currency,
                                   price_discount, average_cost_currency)))

                elif product_currency == 'MXN':
                    conversion = list_price[x].price * rounding
                    calc = discount_lines[x].discount * conversion / 100
                    price_discount = conversion - calc
                    if price_discount < average_cost:
                        self.product_id = False
                        raise exceptions.Warning(_("""
                            '%s'\n
                            Discount price is lower than the average
                            cost of the product\n
                            Average cost: '%.2f %s'\n
                            Discount: '%.2f%s', Level '%s'\n
                            List price: '%.2f %s'\n
                            Discount price: '%.2f %s'\n
                            List price(converted): '%.2f %s'\n
                            Discount price(converted): '%.2f %s'\n
                            """ % (self.line_ids[x].product_id.name,
                                   average_cost, average_cost_currency,
                                   discount_lines[x].discount,
                                   percentage_symbol,
                                   discount_lines[x].level_line_id.id,
                                   list_price[x].price, list_price_currency,
                                   discount_list_price, list_price_currency,
                                   conversion, average_cost_currency,
                                   price_discount, average_cost_currency)))

            else:
                if discount_list_price < average_cost:
                    self.product_id = False
                    raise exceptions.Warning(_("""
                            '%s'\n
                            Discount price is lower than the average
                            cost of the product\n
                            Average cost: '%.2f %s'\n
                            Discount: '%.2f%s', Level: '%s'\n
                            Price: '%.2f %s',\n
                            Discount price: '%.2f %s'\n
                            """ % (self.line_ids[x].product_id.name,
                                   average_cost, average_cost_currency,
                                   discount_lines[x].discount,
                                   percentage_symbol,
                                   discount_lines[x].level_line_id.id,
                                   list_price[x].price, list_price_currency,
                                   discount_list_price, list_price_currency)))

        if level_ids:
            for record in level_ids:
                for calc_record in record:
                    calc = calc_record.discount
                    calc_values.append(calc)
        if line_ids and apply_levels:
            for record in line_ids:
                y = 0
                x = list(record.product_id.product_price_levels_nopop)
                for price_record in x:
                    price = price_record.price
                    price_values.append(price)
                    discount = calc_values[y] * price / 100
                    promo_price = price - discount
                    promo_values.append(promo_price)
                    y += 1
                values.append((1, record.id, {
                    'level1_price': price_values[0],
                    'level2_price': price_values[1],
                    'level3_price': price_values[2],
                    'level4_price': price_values[3],
                    'level5_price': price_values[4],
                    'level6_price': price_values[5],
                    'level7_price': price_values[6],
                    'level8_price': price_values[7],
                    'level9_price': price_values[8],
                    'level10_price': price_values[9],
                    'level1_discount': calc_values[0],
                    'level2_discount': calc_values[1],
                    'level3_discount': calc_values[2],
                    'level4_discount': calc_values[3],
                    'level5_discount': calc_values[4],
                    'level6_discount': calc_values[5],
                    'level7_discount': calc_values[6],
                    'level8_discount': calc_values[7],
                    'level9_discount': calc_values[8],
                    'level10_discount': calc_values[9],
                    'level1_promo': promo_values[0],
                    'level2_promo': promo_values[1],
                    'level3_promo': promo_values[2],
                    'level4_promo': promo_values[3],
                    'level5_promo': promo_values[4],
                    'level6_promo': promo_values[5],
                    'level7_promo': promo_values[6],
                    'level8_promo': promo_values[7],
                    'level9_promo': promo_values[8],
                    'level10_promo': promo_values[9],
                }))
                del price_values[:]
                del promo_values[:]
        self.line_ids = values
        return self.env['product.levels.window']. \
            level_prices(line_ids, apply_levels, level_ids)

    @api.onchange('apply_levels')
    def _onchange_apply_levels(self):
        values = []
        lvl_values = []
        if self.apply_levels:
            self.update({
                'is_level': _("Yes"),
                'discount': 0
            })
            for record in self.line_ids:
                values.append((1, record.id, {
                    'disc': False,
                    'disc_price': False,
                    'disc_percentage': False,
                }))
        else:
            self.update({'is_level': "No"})
            for record in self.level_ids:
                lvl_values.append((1, record.id, {
                    'discount': False,
                }))
        if values:
            self.line_ids = values
        elif lvl_values and self.folio:
            self.level_ids = lvl_values

    @api.multi
    def update_discount(self):
        values = []
        discount = self.discount
        company_object = self.env['res.company']
        company_currency = company_object.search([
            ('id', '=', '1')]
        ).currency_id.name
        currency_object = self.env['res.currency']
        currency_rate = currency_object.search([('name', '=', 'USD')])
        apply_levels = self.apply_levels
        repeated_products = []

        denied_products = []
        average_cost_d = []
        discount_d = []
        list_price_d = []
        discount_price_d = []
        list_price_converted_d = []
        discount_price_converted_d = []

        aproved_products = []
        average_cost_a = []
        discount_a = []
        list_price_a = []
        discount_price_a = []
        list_price_converted_a = []
        discount_price_converted_a = []

        for line in self.line_ids:
            product_currency = line.product_id.list_price_currency.name
            average_cost = line.product_id.standard_price
            list_price = line.product_id.list_price
            average_cost_currency = \
                line.product_id.average_cost_currency_ro.name
            list_price_currency = line.product_id.list_price_currency.name

            discount_list_price_calc = list_price * self.discount / 100
            list_price_discount = list_price - discount_list_price_calc
            rounding = currency_rate.rate_sale_silent

            if product_currency != company_currency:
                if product_currency == 'USD':
                    conversion = list_price / rounding
                    calc = self.discount * conversion / 100
                    discount_price = conversion - calc
                    if discount_price < average_cost:
                        self.get_denied_products(
                            denied_products, line, average_cost,
                            average_cost_currency, average_cost_d, discount,
                            discount_d, list_price, list_price_currency,
                            list_price_d, list_price_discount,
                            discount_price_d, conversion,
                            list_price_converted_d, discount_price,
                            discount_price_converted_d, True)
                        continue

                elif product_currency == 'MXN':
                    conversion = list_price * rounding
                    calc = self.discount * conversion / 100
                    discount_price = conversion - calc
                    if discount_price < average_cost:
                        self.get_denied_products(
                            denied_products, line, average_cost,
                            average_cost_currency,
                            average_cost_d, discount, discount_d, list_price,
                            list_price_currency, list_price_d,
                            list_price_discount, discount_price_d, conversion,
                            list_price_converted_d, discount_price,
                            discount_price_converted_d, True
                        )
                        continue
            else:
                if list_price_discount < average_cost:
                    conversion = False
                    discount_price = False
                    self.get_denied_products(
                        denied_products, line, average_cost,
                        average_cost_currency,
                        average_cost_d, discount, discount_d, list_price,
                        list_price_currency, list_price_d,
                        list_price_discount, discount_price_d, conversion,
                        list_price_converted_d, discount_price,
                        discount_price_converted_d, False
                    )
                    continue

            if not denied_products and not self.apply_levels:
                price = line.price
                disc, disc_price = \
                    line.calculate_discount(price, self.discount)

                values.append((1, line.id, {
                        'disc_percentage': self.discount,
                        'disc': disc,
                        'disc_price': disc_price,
                    }
                ))

        if denied_products and self.apply_levels is False:
            if self.line_ids:
                self.discount = self.line_ids[0].disc_percentage
            else:
                self.discount = False
            return \
                self.env['repeated.products.window']. \
                show_wizard_repeated_products(
                    repeated_products, aproved_products,
                    denied_products, average_cost_d, discount_d, list_price_d,
                    discount_price_d, list_price_converted_d,
                    discount_price_converted_d,
                    average_cost_a, discount_a,
                    list_price_a, discount_price_a, list_price_converted_a,
                    discount_price_converted_a
                )
        else:
            self.line_ids = values

    @api.one
    def state_cancel(self):
        self.cancel_user = self.env.user
        self.cancel_date = datetime.datetime.now()
        self.write({
            'state': 'cancel'
        })

    @api.one
    def state_active(self):
        # import pdb; pdb.set_trace()
        if not self.end_date:
            raise exceptions.Warning(_('Please set up an end date first'))
        self.start_date = datetime.datetime.now()
        self.write({
            'state': 'active'
        })

    def invisible_active_button(self):
        if self.folio:
            for record in self:
                record.state_active_button = 'on'
        else:
            for record in self:
                record.state_active_button = 'off'

    def invisible_cancel_button(self):
        if self.folio:
            for record in self:
                record.state_cancel_button = 'on'
        else:
            for record in self:
                record.state_cancel_button = 'off'

    @api.multi
    def get_filters(self):
        values = []
        promotions = []
        products = []
        product_records = []
        product_ids = []
        repeated_products = []

        denied_products = []
        average_cost_d = []
        discount_d = []
        list_price_d = []
        discount_price_d = []
        list_price_converted_d = []
        discount_price_converted_d = []

        aproved_products = []
        average_cost_a = []
        discount_a = []
        list_price_a = []
        discount_price_a = []
        list_price_converted_a = []
        discount_price_converted_a = []

        section_id = self.section.id
        line_id = self.line.id
        brand_id = self.brand.id
        serial_id = self.serial.id
        branch_office_id = self.branch_office
        segment_id = self.segment
        company_object = self.env['res.company']
        company_currency = company_object.search([
            ('id', '=', '1')]).currency_id.name
        currency_object = self.env['res.currency']
        currency_rate = currency_object.search([('name', '=', 'USD')])
        apply_levels = self.apply_levels

        if section_id or line_id or brand_id or serial_id:
            product_records = self.env['product.template']. \
                search(['|', '|', '|',
                        ('section', '=', section_id), ('line', '=', line_id),
                        ('brand', '=', brand_id), ('serial', '=', serial_id),
                        ('active', '=', True), ('sale_ok', '=', True), \
                        ('type', 'not in', ['service'])])

            promotions_object = self.env['product.promotion']
            promotions = promotions_object.search([
                ('state', 'in', ['active', 'draft'])
            ])

        if not product_records:
            raise exceptions.Warning(
                _("""
                    Product(s) not found in your search criteria
                """)
                )

        for line in product_records:

            products_object = self.env['product.promotion.lines']
            products = products_object.search([
                    ('product_id', '=', line.id),
                    ('promotion_id', 'in', promotions.ids)
                ], limit=1).product_id.id

            if products == line.id:
                repeated_products.append(products)
                continue

            """Here is where we validate if the discount price applied is
            less than the average cost of the product"""

            discount = self.discount
            product_currency = line.list_price_currency.name
            average_cost = line.standard_price
            list_price = line.list_price
            average_cost_currency = line.average_cost_currency_ro.name
            list_price_currency = line.list_price_currency.name

            discount_list_price_calc = list_price * discount / 100
            list_price_discount = list_price - discount_list_price_calc
            rounding = currency_rate.rate_sale_silent
            if product_currency != company_currency:
                if product_currency == 'USD':
                    conversion = list_price / rounding
                    calc = discount * conversion / 100
                    discount_price = conversion - calc
                    if discount_price < average_cost:
                        self.get_denied_products(
                            denied_products, line, average_cost,
                            average_cost_currency, average_cost_d, discount,
                            discount_d, list_price, list_price_currency,
                            list_price_d, list_price_discount,
                            discount_price_d, conversion,
                            list_price_converted_d, discount_price,
                            discount_price_converted_d, True
                        )
                        continue
                    if discount_price > average_cost:
                        self.get_aproved_products(
                            aproved_products, line, average_cost,
                            average_cost_currency, average_cost_a, discount,
                            discount_a, list_price, list_price_currency,
                            list_price_a, list_price_discount,
                            discount_price_a, conversion,
                            list_price_converted_a, discount_price,
                            discount_price_converted_a, True
                        )
                        pass

                elif product_currency == 'MXN':
                    conversion = list_price * rounding
                    calc = discount * conversion / 100
                    discount_price = conversion - calc
                    if discount_price < average_cost:
                        self.get_denied_products(
                            denied_products, line, average_cost,
                            average_cost_currency, average_cost_d, discount,
                            discount_d, list_price, list_price_currency,
                            list_price_d, list_price_discount,
                            discount_price_d, conversion,
                            list_price_converted_d, discount_price,
                            discount_price_converted_d, True
                        )
                        continue
                    if discount_price > average_cost:
                        self.get_aproved_products(
                            aproved_products, line, average_cost,
                            average_cost_currency, average_cost_a, discount,
                            discount_a, list_price, list_price_currency,
                            list_price_a, list_price_discount,
                            discount_price_a, conversion,
                            list_price_converted_a, discount_price,
                            discount_price_converted_a, True
                        )
                        pass

            else:
                if list_price_discount < average_cost:
                    conversion = False
                    discount_price = False
                    self.get_denied_products(
                        denied_products, line, average_cost,
                        average_cost_currency,
                        average_cost_d, discount, discount_d, list_price,
                        list_price_currency, list_price_d,
                        list_price_discount, discount_price_d, conversion,
                        list_price_converted_d, discount_price,
                        discount_price_converted_d, False
                    )
                    continue
                if list_price_discount > average_cost:
                    conversion = False
                    discount_price = False
                    self.get_aproved_products(
                        aproved_products, line, average_cost,
                        average_cost_currency,
                        average_cost_a, discount, discount_a, list_price,
                        list_price_currency, list_price_a,
                        list_price_discount, discount_price_a, conversion,
                        list_price_converted_a, discount_price,
                        discount_price_converted_a, False
                    )
                    pass

            if self.apply_levels is False:
                values.append((0, 0, {
                        'product_id': line,
                        'branch_office': branch_office_id,
                        'segment': segment_id,
                    }
                ))

        if values:
            self.line_ids = values

        if repeated_products or denied_products and self.apply_levels is False:
            return \
                self.env['repeated.products.window']. \
                show_wizard_repeated_products(
                    repeated_products, aproved_products,
                    denied_products, average_cost_d, discount_d, list_price_d,
                    discount_price_d, list_price_converted_d,
                    discount_price_converted_d, average_cost_a, discount_a,
                    list_price_a, discount_price_a, list_price_converted_a,
                    discount_price_converted_a
                )

    @api.one
    def delete_all(self):
        self.line_ids.unlink()

    @api.one
    def delete_filters(self):
        self.section = False
        self.line = False
        self.brand = False
        self.serial = False
        self.branch_office = False
        self.segment = False

    @api.model
    def create(self, vals):
        obj = self.env['internal.product.promotion.settings'].search(
            [], limit=1
        )
        date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        start_date = vals.get('start_date')
        end_date = vals.get('end_date')
        apply_levels = vals.get('apply_levels')
        level_ids = vals.get('level_ids')
        if start_date:
            if start_date < date_now:
                start_date = False
                raise exceptions.Warning(
                    _("The current start date is behind today's date/hour"))
        if end_date:
            if end_date < date_now:
                end_date = False
                raise exceptions.Warning(
                    _("The current end date is behind today's date/hour"))
        if 'discount' in vals and apply_levels is False:
            if vals['discount'] <= 0.0:
                raise exceptions.Warning(
                    _('Discount can´t be lower or equal than zero')
                    )
            if vals['discount'] > obj.promotion_limit_discount:
                raise exceptions.Warning(
                    _("The limit discount is %s" % obj.promotion_limit_discount))
        if apply_levels:
            for x in range(10):
                if level_ids[x][2].values()[1] <= 0.0:
                    raise exceptions.Warning(
                        _("""Discount can´t be lower or equal than zero\n
                            in discount level #%s""" % (x+1))
                        )
        vals['folio'] =\
            self.env['ir.sequence'].next_by_code('product.promotion.folio')
        return super(ProductPromotion, self).create(vals)

    @api.one
    def write(self, vals):
        date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        start_date = vals.get('start_date')
        end_date = vals.get('end_date')
        apply_levels = vals.get('apply_levels')
        level_ids = vals.get('level_ids')
        discount = vals.get('discount')

        if discount:
            obj = self.env['internal.product.promotion.settings'].search(
                [], limit=1
            )
            if discount > obj.promotion_limit_discount:
                raise exceptions.Warning(
                    _("The limit discount is %s" % obj.promotion_limit_discount))

        if start_date:
            if start_date < date_now:
                start_date = False
                raise exceptions.Warning(
                    _("The current start date is behind today's date/hour"))
        if end_date:
            if end_date < date_now:
                end_date = False
                raise exceptions.Warning(
                    _("The current end date is behind today's date/hour"))
        if 'discount' in vals:
            if not apply_levels:
                if vals['discount'] <= 0.0:
                    raise exceptions.Warning(
                        _('Discount can´t be lower or equal than zero'))
        elif apply_levels is False and 'discount' not in vals:
            raise exceptions.Warning(
                _('Discount can´t be lower or equal than zero'))
        if apply_levels is True and 'level_ids' not in vals:
            raise exceptions.Warning(
                _('Level Discounts can´t be lower or equal than zero'))
        if level_ids:
            if apply_levels:
                for x in range(10):
                    if level_ids[x][2]:
                        if level_ids[x][2].values()[0] <= 0.0 or\
                                level_ids[x][2].values()[0] is False:
                                    raise exceptions.Warning(
                                        _("""
                                            Discount can´t be lower or
                                            equal than zero\n
                                            in discount level #%s""" % (x+1))
                                        )
                    elif level_ids[x][2] is False:
                        raise exceptions.Warning(
                            _("""Discount can´t be lower or equal than zero\n
                                in discount level #%s""" % (x+1))
                            )
            elif apply_levels is None:
                for x in range(10):
                    if level_ids[x][2]:
                        if level_ids[x][2].values()[0] <= 0.0 or False:
                            raise exceptions.Warning(
                                    _("""Discount can´t be lower
                                        or equal than zero\n
                                        in discount level #%s""" % (x+1))
                                    )
            elif apply_levels and level_ids is None:
                raise exceptions.Warning(_("""Discount levels can't be lower
                than zero"""))
            elif apply_levels is False:
                pass
        return super(ProductPromotion, self).write(vals)

    def get_denied_products(
            self, denied_products, line, average_cost, average_cost_currency,
            average_cost_d, discount, discount_d, list_price,
            list_price_currency, list_price_d,
            list_price_discount, discount_price_d, conversion,
            list_price_converted_d, discount_price,
            discount_price_converted_d, param=True):

        denied_products.append(line.id)
        average_cost_string = str(average_cost)
        average_cost_currency_string = str(average_cost_currency)
        string_result = \
            average_cost_string + " " + average_cost_currency_string
        average_cost_d.append(string_result)
        discount_string = str(discount)
        string_result = discount_string + '%'
        discount_d.append(string_result)
        list_price_string = str(list_price)
        list_price_currency_string = str(list_price_currency)
        string_result = list_price_string + " " + list_price_currency_string
        list_price_d.append(string_result)
        discount_price_string = str(list_price_discount)
        discount_price_currency = str(list_price_currency)
        string_result = discount_price_string + " " + discount_price_currency
        discount_price_d.append(string_result)
        if param:
            conversion_string = str(conversion)
            conversion_currency_string = str(average_cost_currency)
            string_result = \
                conversion_string + " " + conversion_currency_string
            list_price_converted_d.append(string_result)
            price_discount = str(discount_price)
            price_discount_string = str(average_cost_currency)
            string_result = price_discount + " " + price_discount_string
            discount_price_converted_d.append(string_result)
        else:
            list_price_converted_d.append(False)
            discount_price_converted_d.append(False)

    def get_aproved_products(
            self, aproved_products, line, average_cost, average_cost_currency,
            average_cost_a, discount, discount_a, list_price,
            list_price_currency, list_price_a,
            list_price_discount, discount_price_a, conversion,
            list_price_converted_a, discount_price,
            discount_price_converted_a, param=True):

        aproved_products.append(line.id)
        average_cost_string = str(average_cost)
        average_cost_currency_string = str(average_cost_currency)
        string_result =\
            average_cost_string + " " + average_cost_currency_string
        average_cost_a.append(string_result)
        discount_string = str(discount)
        string_result = discount_string + '%'
        discount_a.append(string_result)
        list_price_string = str(list_price)
        list_price_currency_string = str(list_price_currency)
        string_result = list_price_string + " " + list_price_currency_string
        list_price_a.append(string_result)
        discount_price_string = str(list_price_discount)
        discount_price_currency = str(list_price_currency)
        string_result = discount_price_string + " " + discount_price_currency
        discount_price_a.append(string_result)
        if param:
            conversion_string = str(conversion)
            conversion_currency_string = str(average_cost_currency)
            string_result =\
                conversion_string + " " + conversion_currency_string
            list_price_converted_a.append(string_result)
            price_discount = str(discount_price)
            price_discount_string = str(average_cost_currency)
            string_result = price_discount + " " + price_discount_string
            discount_price_converted_a.append(string_result)
        else:
            list_price_converted_a.append(False)
            discount_price_converted_a.append(False)

    @api.cr_uid_context
    def fields_view_get(
            self, cr, uid, view_id=None, view_type='form',
            context=None, toolbar=False, submenu=False):
        result = super(ProductPromotion, self).fields_view_get(
            cr, uid, view_id, view_type, context, toolbar, submenu
        )

        env = api.Environment(cr, uid, [])
        self.env = env
        if view_type == 'form':
            from lxml import etree
            root = etree.fromstring(result['arch'])
            line_ids_xml = root.xpath('//form/body/field[@name="line_ids"]')[0]
            conf_obj = self.env['internal.product.promotion.settings']
            settings = conf_obj.search([], limit=1)

            if settings.promotion_add_products:
                line_ids_xml.set(
                    'modifiers',
                    '{"readonly": [["state", "in", ["expire","cancel"]]]}'
                )
            else:
                line_ids_xml.set(
                    'modifiers',
                    '{"readonly": \
                    [["state", "in", ["active","expire","cancel"]]]}'
                )
            result['arch'] = etree.tostring(root)

        return result
