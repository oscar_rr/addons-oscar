# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, fields, models


class CreditExtensionLine(models.Model):
    _name = 'credit.extension.line'
    date_line = fields.Datetime('Date')
    partner_id = fields.Many2one('res.partner', 'Client')
    amount = fields.Float('Authorize amount')
