# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, fields, models


class CreditExtensionLineLog(models.Model):
    _name = 'credit.extension.line.log'
    order_id = fields.Many2one('sale.order', 'Order')
    amount_extension = fields.Float('amount')
    partner_id = fields.Many2one('res.partner', 'Partner')
    order_status = fields.Selection([
        ('C', 'Cancelada'),
        ('A', 'Activa'),
        ('P', 'Pagada'),
    ])
