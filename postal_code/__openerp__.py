# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Postal Codes assignment for Contacts',
    'version': '0.1',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'category': 'Contacts',
    'description': """
        Postal Codes management adding the Postal Code number and related data
        such as State, City, Municipality, Colony and Country, being able to
        add a Postal code and autofill its information when creating or
        modifying a new contact such as Customers or Vendors.
         """,
    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': [
        'l10n_mx_partner_address',
        'base',
        'city',
        'configuracion',
        'l10n_mx_cities',
        'l10n_mx_states'
    ],
    'data': [
             'security/ir.model.access.csv',
             'data/missing_cities.xml',
             # 'data/res.settlement.type.csv',
             # 'data/res.postal.code.csv',
             # 'data/res.state.city.colony.csv',
             'views/res_partner.xml',
             'views/res_country.xml',
             'views/res_colony_view.xml',
             'views/res_settlement_type_view.xml',
             'views/res_postal_code_view.xml',
             'views/res_partner_kanban_inherit.xml',
             'views/res_company_inherit.xml',
             'views/sale_order_view.xml',
             'views/inherit_contact.xml',
    ],
    'installable': True,
    'active': False,
}
