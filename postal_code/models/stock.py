
from openerp.osv import fields, osv
from datetime import datetime, timedelta
import time
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import models, api, _
from openerp import SUPERUSER_ID
from openerp.exceptions import Warning,ValidationError


class stock_move(osv.osv):
    _inherit = 'stock.move'

    def _get_invoice_line_vals(self, cr, uid, move, partner, inv_type, context=None):
        res = super(stock_move, self)._get_invoice_line_vals(cr, uid, move=move, partner=partner, inv_type=inv_type, context=None)
        ship_partner = move.picking_id.partner_id
        if ship_partner:
            if ship_partner.zip:
                if ship_partner.zip.account_prod_id:
                    account_id = ship_partner.zip.account_prod_id.id
                    res['account_id'] = account_id
            # print 'Este es el RES ********************'
            # print res
        return res

class stock_picking(osv.osv):
    _inherit = "stock.picking"


    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        res = super(stock_picking, self)._get_invoice_vals(cr, uid, key=key, inv_type=inv_type, journal_id=journal_id, move=move, context=None)
        ship_partner = move.picking_id.partner_id

        if ship_partner:
            if ship_partner.zip:
                if ship_partner.zip.customer_account_id:
                    account_id = ship_partner.zip.customer_account_id.id
                    res['account_id'] = account_id
                res['res_shipping'] = ship_partner.id
        return res
