# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports

from datetime import datetime, timedelta
import time
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import models, api, _
from openerp import SUPERUSER_ID
from openerp.exceptions import Warning,ValidationError


class SaleOrder(models.Model):

    _inherit = 'sale.order'

    # @api.constrains('partner_shipping_id')
    # def _check_zip(self):
    #     for record in self:
    #         if record.partner_shipping_id:
    #             if not record.partner_shipping_id.zip:
    #                 raise ValidationError("Your partner for shipping haven't a zip: %s" % record.partner_shipping_id.name)

class sale_order_line(models.Model):

    _inherit = 'sale.order.line'
    _description = 'Sales Order Line'

    @api.multi
    def product_id_change(self,pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
        res = super(sale_order_line, self).product_id_change(pricelist=pricelist, product=product, qty=qty, uom=uom, qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id, lang=lang, update_tax=update_tax, date_order=date_order, packaging=packaging, fiscal_position=fiscal_position, flag=flag, context=context)
        to_ship = self.env.context.get('to_ship')
        res_partner_obj = self.env['res.partner']
        prtner_to_ship = res_partner_obj.browse(to_ship)
        tax_array = []
        zip_taxes_ids = prtner_to_ship.zip.tax_prod_ids
        for tax in zip_taxes_ids:
            tax_array.append(tax.id)
        if tax_array: 
            res['value'].update({'tax_id': tax_array}) 
        return res

    @api.model
    def _prepare_order_line_invoice_line(self, line, account_id):
        res = super(sale_order_line, self)._prepare_order_line_invoice_line(line, account_id)
        if line.order_id.partner_shipping_id:
            if line.order_id.partner_shipping_id.zip:
                if  line.order_id.partner_shipping_id.zip.account_prod_id:
                    account_id=line.order_id.partner_shipping_id.zip.account_prod_id.id
                    res['account_id']=account_id
        return res