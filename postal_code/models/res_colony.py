# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _
import openerp.tools as tools
import logging


class ResStateCityColony(models.Model):
    _inherit = 'mail.thread'
    _name = 'res.state.city.colony'
    _rec_name = 'name'

    name = fields.Char(
        string=_('Settlement'),
        help=_('Name of Settlement')
    )
    settlement_id = fields.Many2one(
        comodel_name='res.settlement.type',
        string=_('Type of Settlement'),
        help=_('Type of Settlement')
    )
    postal_id = fields.Many2one(
        comodel_name='res.postal.code',
        string=_('Zip code'),
        required=True,
        help=_('Number of zip code')
    )
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=_('Country'),
        required=True,
        help=_('Name of Country')
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        string=_('State'),
        required=True,
        help=_('Name of State')
    )
    city_id = fields.Many2one(
        comodel_name='res.country.state.city',
        string=_('City/Municipality'),
        help=_('Name of City/Municipality')
    )

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            return {
                'domain': {
                    'state_id': [('country_id', '=', self.country_id.id)]
                    }
                }
        else:
            return {'domain': {'state_id': []}}

    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            return {
                'domain': {
                    'city_id': [('state_id', '=', self.state_id.id)]
                    }
                }
        else:
            return {'domain': {'city_id': []}}

    @api.model
    def create(self, vals):
        if 'postal_id' in vals and 'country_id' in vals and 'state_id' in vals\
        and 'city_id' in vals:
            country_id = vals.get('country_id')
            state_id = vals.get('state_id')
            city_id = vals.get('city_id')
            name = vals.get('name')
            settlement_id = vals.get('settlement_id')
            test_passed = False
            postal_code = self.env['res.postal.code'].browse(vals.get('postal_id'))
            if postal_code.country_id.id and country_id != postal_code.country_id.id:
                raise exceptions.Warning(_("""The country you selected cannot
                                           be assigned to the zip code
                                           %s"""%postal_code.name))
            if postal_code.state_id.id and state_id != postal_code.state_id.id:
                raise exceptions.Warning(_("""The state you selected cannot
                                           be assigned to the zip code
                                           %s"""%postal_code.name))
            if postal_code.city_id.id and city_id != postal_code.city_id.id:
                raise exceptions.Warning(_("""The city you selected cannot
                                           be assigned to the zip code
                                           %s"""%postal_code.name))
            else:
                test_passed = True
            if test_passed:
                for line in postal_code.line_ids:
                    if country_id == line.country_id.id and state_id == line.state_id.id\
                        and city_id == line.city_id.id and name == line.name\
                        and settlement_id == line.settlement_id.id:
                        raise exceptions.Warning(_("""
                                                    This settlement already exists
                                                    in zip code: %s
                                                   """%postal_code.name))
        return super(ResStateCityColony, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'name' in vals or 'settlement_id' in vals:
            name = vals.get('name') if vals.get('name') else self.name
            settlement_id = vals.get('settlement_id') \
                if vals.get('settlement_id') else self.settlement_id.id
            for line in self.postal_id.line_ids:
                if name == line.name and settlement_id == line.settlement_id.id:
                    raise exceptions.Warning(_("""
                                                This settlement already exists
                                                in zip code: %s
                                               """%self.postal_id.name))
        return super(ResStateCityColony, self).write(vals)
