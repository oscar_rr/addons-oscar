# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models
from openerp.osv import osv

class InheritResCompany(models.Model):
    _inherit = 'res.company'

    street2 = fields.Char(
        compute='get_res_partner_values',
        string='street2',
        readonly=True
    )
    zip = fields.Char(
        compute='get_res_partner_values',
        string="zip",
        readonly=True
    )
    city = fields.Char(
        compute='get_res_partner_values',
        string="city",
        readonly=True
    )

    def get_res_partner_values(self):
        part_obj = self.env['res.partner']
        partner_id = part_obj.search([('id','=',self.partner_id.id)])
        if partner_id:
            self.street2 = partner_id.street2.name
            self.city = partner_id.city_id.name
            self.zip = partner_id.zip.name
        else:
            return False

class bank(osv.osv):
    _inherit = "res.partner.bank"
    
    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        result = {}
        if partner_id is not False:
            # be careful: partner_id may be a NewId
            part = self.pool['res.partner'].browse(cr, uid, [partner_id], context=context)
            result['owner_name'] = part.name
            result['street'] = part.street or False
            result['city'] = part.city or False
            result['zip'] =  part.zip.name or False
            result['country_id'] =  part.country_id.id
            result['state_id'] = part.state_id.id
        return {'value': result}