# -*- coding:utf-8 -*-
from openerp import models, fields, api, _
import ast


class ProductSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    _name = 'product.meli.settings'

    default_locations = fields.Many2many('stock.warehouse', string="Almacenes")
    default_full_locations = fields.Many2many('stock.warehouse', 'stock_warehouse_meli_settings', string="Almacenes Full")
    default_full_location_orders = fields.Many2one('stock.warehouse', 'Almacen para ordenes Full')
    default_res_partner = fields.Many2one('res.partner', 'Cliente para ordenes')

    @api.model
    def get_default_locations(self, fields):
        conf = self.env['ir.config_parameter']
        ids = conf.get_param('mercadolibre.locations')
        ids_full = conf.get_param('mercadolibre.full.locations')
        warehouse_full = conf.get_param('mercadolibre.full.warehouse')
        res_partner = conf.get_param('mercadolibre.res.partner')
        response = {}
        if ids_full:
            response['default_full_locations'] = [(6, 0, ast.literal_eval(ids_full))]
        else:
            response['default_full_locations'] = []

        if ids:
            response['default_locations'] = [(6, 0, ast.literal_eval(ids))]
        else:
            response['default_locations'] = []

        if warehouse_full:
            response['default_full_location_orders'] = int(warehouse_full)
        else:
            response['default_full_location_orders'] = False

        if res_partner:
            response['default_res_partner'] = int(res_partner)
        else:
            response['default_res_partner'] = False

        return response

    @api.multi
    def set_params(self):
        conf = self.env['ir.config_parameter']
        conf.set_param('mercadolibre.full.locations', self.default_full_locations.ids)
        conf.set_param('mercadolibre.locations', self.default_locations.ids)
        conf.set_param('mercadolibre.full.warehouse', self.default_full_location_orders.id)
        conf.set_param('mercadolibre.res.partner', self.default_res_partner.id)
