from openerp import models, fields, api, _
from openerp.exceptions import RedirectWarning,Warning
from ..amazon_emipro_api.api import AmazonAPI
from ..amazon_emipro_api.mws import Recommendations
from datetime import datetime
from collections import defaultdict

class amazon_process_import_export(models.TransientModel):
    _name = 'amazon.process.import.export'
   
    instance_ids = fields.Many2many("amazon.instance.ept",'amazon_instance_import_export_rel','process_id','instance_id',"Instances")
    browse_node_ids = fields.Many2many("amazon.browse.node.ept",'amazon_browse_node_import_export_rel',
                                       'process_id','browse_node_id',"Browse Nodes"
                                       )
    #instance_id = fields.Many2one("amazon.instance.ept","Instance")
    
    import_sale_order = fields.Boolean('Sale order')
    import_browse_node = fields.Boolean('Browse Node')
    
    export_product = fields.Boolean('Export Products')
    export_product_price = fields.Boolean('Update Product Price')
    export_product_images = fields.Boolean('Update Product Images')
    export_inventory = fields.Boolean('Export Inventory')
    export_order_status = fields.Boolean('Update Order Status')
    
        
    @api.model
    def default_get(self,fields):
        res = super(amazon_process_import_export,self).default_get(fields)
        if 'instance_ids' in fields:
            instance_ids = self.env['amazon.instance.ept'].search([])
            res.update({'instance_ids':[(6,0,instance_ids.ids)]})
        return res
    
    @api.multi
    def import_export_processes(self):
        amazon_product_obj=self.env['amazon.product.ept']
        seller_import_order_marketplaces = defaultdict(list)
        seller_export_order_marketplaces = defaultdict(list)
        for instance in self.instance_ids:
            if self.import_sale_order:
                seller_import_order_marketplaces[instance.seller_id].append(instance.market_place_id)
            if self.import_browse_node:
                browse_nodes = self.browse_node_ids and self.browse_node_ids
                self.import_category(browse_nodes)
            if self.export_product:
                amazon_products=amazon_product_obj.search([('instance_id','=',instance.id)])
                if amazon_products:
                    amazon_product_obj.export_product_amazon(instance,amazon_products)
                
            if self.export_product_price:
                amazon_products=amazon_product_obj.search([('instance_id','=',instance.id),('exported_to_amazon','=',True)])
                if amazon_products:
                    amazon_products.update_price(instance)
            if self.export_product_images:
                amazon_products=amazon_product_obj.search([('instance_id','=',instance.id),('exported_to_amazon','=',True)])
                amazon_products.update_images(instance)
                instance.write({'image_last_sync_on':datetime.now()})
                
            if self.export_inventory:
                instance.export_stock_levels()
            if self.export_order_status:
                seller_export_order_marketplaces[instance.seller_id].append(instance.market_place_id)
                
        sale_order_obj = self.env['amazon.sale.order.ept']
        if seller_import_order_marketplaces:
            for seller,marketplaces in seller_import_order_marketplaces.iteritems():
                sale_order_obj.import_sales_order(seller,marketplaces)
        if seller_export_order_marketplaces:
            for seller,marketplaces in seller_export_order_marketplaces.iteritems():
                sale_order_obj.update_order_status(seller,marketplaces)
        
        return True

    @api.multi
    def import_category(self,root_nodes):
        browse_node=self.env['amazon.browse.node.ept']                            
        for node in root_nodes:
            if not node.instance_id.pro_advt_access_key or not node.instance_id.pro_advt_scrt_access_key or not node.instance_id.pro_advt_associate_tag:
                action = self.env.ref('amazon_ept.action_amazon_config')
                msg = _('You have not configure Product Advertising Account, You should configure it. \nPlease go to Amazon Configuration.')
                raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))
            instance=AmazonAPI(str(node.instance_id.pro_advt_access_key),str(node.instance_id.pro_advt_scrt_access_key),aws_associate_tag=str(node.instance_id.pro_advt_associate_tag),region=str(node.instance_id.country_id.amazon_marketplace_code or node.instance_id.country_id.code),MaxQPS=0.5,Timeout=10)
            ancestor=False
            results=[]
            try:
                results=instance.browse_node_lookup(BrowseNodeId=int(node.ama_category_code))
            except Exception,e:
                pass
            if not results:
                continue
                             
            for result in results:
                if result.is_category_root:
                    ancestor=browse_node.check_ancestor_exist_or_not(result,node)
                    try:                                                                                             
                        for children in result.children:
                            parent=ancestor and ancestor.id or node.id
                            browse_node.check_children_exist_or_not(children, node, parent)
                    except Exception,e:
                        raise Warning(str(e))   
        return True 