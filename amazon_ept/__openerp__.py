

{
    'name': 'Amazon Odoo Connector',
    'version': '8.0',
    'category': 'Sales',
    'summary' : 'Integrate & Manage your all your Amazon seller account operations from Odoo',
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com/',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
    'license': 'Other proprietary',
    'depends': ['auto_invoice_workflow_ept', 'delivery', 'web','sale_stock', 'postal_code'],
    'data': [
             'security/res_groups.xml',
             'security/ir.model.access.csv',
             'view/res_config_view.xml',
             'view/product_brand_view.xml',
             'view/instance_view.xml',
             'view/amazon_seller.xml',
             'view/base_browse_node.xml',
             'view/product_view.xml',
             'view/browse_node.xml',
             'view/sales_view.xml',
             'view/auto_work_flow_configuration.xml',
             'view/tax_code_view.xml',
             'view/ir_cron.xml',
             'view/stock_view.xml',
             'view/product_wizard_view.xml',
             'view/cancel_order_wizard_view.xml',
             'view/res_country.xml',
             'view/process_import_export.xml',
             'view/order_refund_view.xml',
             'view/feed_submission_history.xml',
             'view/amazon_file_process_job.xml',
             'view/invoice_view.xml',
             'view/web_templates.xml',
             'view/stock_quant_package.xml',
             'view/stock_warehouse.xml',
             'view/settlement_report.xml',
             'view/amazon_report_wizard.xml',
             'view/amazon_transaction.xml',
             'view/operations.xml',
             'report/sale_report_view.xml',
             'view/active_product_listing_view.xml',
             'view/sale.xml',
             'view/account_fiscal_position.xml',
             'view/product_attribute.xml',
             'view/amazon_category.xml',
             'view/amazon_category_data.xml',
             'data/amazon.base.browse.node.ept.csv',
             'data/amazon.operations.ept.csv',
             'data/amazon.uom.type.ept.csv',
             'data/amazon.uom.value.ept.csv',
             'data/amazon.attribute.ept.csv',
             'data/amazon.attribute.value.ept.csv',
             'data/amazon.tax.code.ept.csv',
             'data/amazon.cspia.warning.ept.csv',
             'data/amazon.payment.type.option.ept.csv',
             'data/amazon.tsd.language.ept.csv',
             'data/amazon.tsd.warning.ept.csv',
             'data/amazon.variation.theme.ept.csv',
             ],
    'description': """
This module used to smoothly integrate your Amazon account with Odoo. \n

After installation of our module, you can manager following Amazon operations in Odoo, \n

Our module supports following features related to Amazon. \n

    * Amazon Instance Configuration & Setup \n
    * Multi Amazon Instance setup \n
    * Import Browse Nodes / Categories from Amazon \n
    * Export Product, Inventory, Images \n
    * Update Product price to Amazon \n
    * Live (on the fly) Inventory Export to Amazon \n
    * Import Sale Order from Amazon \n
    * Export Shipping Details with tracking no. \n
    * Cancel & Refund order to Amazon \n
    * Auto Stock/Orders/Shipping status export \n
    * Create Invoice / Payment / Delivery Order in Odoo (Automatically) \n

====================================================================

For support on this module contact us at info@emiprotechnologies.com \n

To subscribe our support packages visit following link, \n

http://www.emiprotechnologies.com/odoo/support \n

Visit following link to find our other cool apps to shape your system . \n

https://www.odoo.com/apps/modules?author=Emipro%20Technologies%20Pvt.%20Ltd. \n

For more information about us, visit www.emiprotechnologies.com \n
    """,
    'images': ['static/description/cover.jpg'],
    'installable': True,
    'auto_install': False,
    'application' : True,
    'live_test_url' :'http://bit.ly/2LWVwr3',
    'price': 450.00,
    'currency': 'EUR',
}
