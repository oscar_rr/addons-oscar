from openerp import models,fields

class stock_warehouse(models.Model):
    _inherit = "stock.warehouse"
    
    seller_id = fields.Many2one('amazon.seller.ept', string='Amazon Seller')
    
    
    def onchange_warehouse_id(self, cr, uid, ids, warehouse_id,partner_id=None ,delivery_id = None,context=None):
        res = super(stock_warehouse,self).onchange_warehouse_id(cr, uid, ids, warehouse_id,context)
        if not res:
            res = {'value': {}}
        context = context and context.copy() or {}
        if warehouse_id and partner_id:
            warehouse = self.browse(cr,uid,warehouse_id,context=context)
            origin_country_id = warehouse.partner_id and warehouse.partner_id.country_id and warehouse.partner_id.country_id.id or False
            origin_country_id = origin_country_id or (warehouse.company_id.partner_id.country_id and warehouse.company_id.partner_id.country_id.id or False)
            context.update({'origin_country_ept':origin_country_id})
            company_id = warehouse.company_id.id
            if not company_id:
                company_id = self._get_default_company(cr, uid, context=context)
            fiscal_position = self.pool['account.fiscal.position'].get_fiscal_position(cr, uid, company_id, partner_id, delivery_id, context=context)
            if fiscal_position:
                res['value']['fiscal_position'] = fiscal_position
        return res
    
    