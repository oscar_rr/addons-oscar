from openerp import models,fields
class res_country(models.Model):
    _inherit="res.country"

    amazon_marketplace_code = fields.Char('Amazon Marketplace Code',size=10,default=False)
    def init(self, cr):
        #Here we can set have_marketplace=true for country that have Amazon marketplace
        cr.execute("""update res_country set amazon_marketplace_code=code
                    where code in ('CA','US','DE','ES','FR','IN','IT','JP','CN', 'MX')""")
        cr.execute("""update res_country set amazon_marketplace_code = 'UK'
                    where code='GB'""") #United Kingdom Amazon marketplace code is UK and Country Code for United Kingdom is GB.


class ResPostalCode(models.Model):
    _inherit="res.postal.code"
