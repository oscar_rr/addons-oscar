from openerp import models,fields,api
class res_partner(models.Model):
    _inherit="res.partner"
    
    def _fields_sync(self, cr, uid, partner, update_values, context=None):
        if context.get('do_not_sync'):
            return True
        else:
            return super(res_partner,self)._fields_sync(cr,uid,partner,update_values,context=context)
        
    def _handle_first_contact_creation(self, cr, uid, partner, context=None):
        if context.get('do_not_sync'):
            return True
        else:
            return super(res_partner,self)._handle_first_contact_creation(cr,uid,partner,context=context)
        