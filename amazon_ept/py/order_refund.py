from openerp import models,fields,api,_
import openerp.addons.decimal_precision as dp
from openerp.exceptions import Warning
from ..amazon_emipro_api.mws import Feeds 
from ..amazon_emipro_api.utils import xml2dict
from tempfile import NamedTemporaryFile
import time
import base64

TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale_refund',
    'in_refund': 'purchase_refund',
}

class amazon_refund_order_lines(models.Model):
    _name="amazon.refund.order.lines"

    @api.multi
    def get_total(self):
        for record in self:
            total=0.0
            total=total+(record.order_line_amount * record.product_qty)
            total=total+record.order_line_tax

            total=total+record.shipping_charge
            total=total+record.shipping_tax
            
                        
            total=total+record.gift_wrap_charge
            total=total+record.gift_wrap_tax
            
            total=total+record.item_promotion_adjust
            total=total+record.shipping_promotion_adjust
            record.total_refund=total
            
    @api.multi
    @api.depends("amazon_refund_id.state")
    def get_state(self):
        for record in self:
            record.state=record.amazon_refund_id.state
    amazon_order_line_id=fields.Many2one("amazon.sale.order.line.ept","Sale Order Line")
    message=fields.Selection([('NoInventory','NoInventory'),
                              ('CustomerReturn','CustomerReturn'),
                              ('GeneralAdjustment','GeneralAdjustment'),
                              ('CouldNotShip','CouldNotShip'),
                              ('DifferentItem','DifferentItem'),
                              ('Abandoned','Abandoned'),
                              ('CustomerCancel','CustomerCancel'),
                              ('PriceError','PriceError'),
                              ],string="Message",default="CustomerReturn")
    amazon_refund_id=fields.Many2one("amazon.order.refund.ept","Refund Id")
    
    amazon_product_id=fields.Many2one("amazon.product.ept","Amazon Product")
    product_id=fields.Many2one("product.product","Odoo Product")
    
    product_qty=fields.Float("Product Qty",digits=dp.get_precision("Product UoS"))
    qty_canceled=fields.Float("Cancel Qty",digits=dp.get_precision("Product UoS"))

    price_subtotal=fields.Float("Order Line SubTotal",digits=(16,2))
    
    order_line_amount=fields.Float("Product Amount (Per Unit)",digits=(16,2))
    order_line_tax=fields.Float("Order Amount Tax",digits=(16,2))
    
    shipping_charge=fields.Float("Shipping Charge",digits=(16,2))
    shipping_tax=fields.Float("Shipping Tax",digits=(16,2))

    gift_wrap_charge=fields.Float("Gift Wrap Charge",digits=(16,2))
    gift_wrap_tax=fields.Float("Gift Wrap Tax",digits=(16,2))

    item_promotion_adjust=fields.Float("Item Promotion Adjust",digits=(16,2))
    item_promotion_id=fields.Char("Item Promotion ID")
    
    shipping_promotion_adjust=fields.Float("Shipping Promotion Adjust",digits=(16,2))
    shipping_promotion_id=fields.Char("Shipping Promotion Id")

    
    total_refund=fields.Float("Total Refund",digits=(16,2),compute="get_total")
    state=fields.Selection([('draft','Draft'),('validate','Validate'),('cancel','Cancel'),('updated_in_amazon','Updated In Amazon')],string="State",compute="get_state")    
    
class amazon_order_refund_ept(models.Model):
    _name="amazon.order.refund.ept"
    _rec_name = 'order_id'
    _inherit = ['mail.thread']
    _order="id desc"
    @api.multi
    def create_return_picking(self):
        for picking in self.order_id.picking_ids:
            if picking.picking_type_code!='outgoing':
                continue
            moves=[]
            move_qty={}
            for line in self.amazon_refund_line_ids:
                if line.amazon_order_line_id.sale_order_line_id:
                    move=self.env['stock.move'].search([('procurement_id.sale_line_id','=',line.amazon_order_line_id.sale_order_line_id.id),('product_id','=',line.product_id.id),('picking_id','=',picking.id)])
                    moves.append(move.id)
                    move_qty.update({move.id:line.qty_canceled})
                result=self.env['stock.return.picking'].with_context({'active_id':picking.id}).default_get(fields=['product_return_moves','move_dest_exists'])
                
                move_dest_exists=[]
                product_return_moves=[]
                if result.get('move_dest_exists',[]):
                    for exist_line in result.get('move_dest_exists',[]):
                        if exist_line.get('move_id') in moves:
                            move_dest_exists.append([0,False,exist_line])
                if result.get('product_return_moves',[]):
                    for move_line in result.get('product_return_moves',[]): 
                        if move_line.get('move_id') in moves:
                            if move_qty.get(move_line.get('move_id'),0.0)>0.0:
                                move_line.update({'quantity':move_qty.get(move_line.get('move_id'),0.0)})
                            product_return_moves.append([0,False,move_line])
                record=self.env['stock.return.picking'].create({'move_dest_exists':move_dest_exists,'product_return_moves':product_return_moves,'invoice_state':'none'})
                result=record.with_context({'active_id':picking.id}).create_returns()
        return True
            
    @api.multi
    def get_picking(self):
        for record in self:
            pickings = self.order_id.picking_ids
            record.picking_ids=pickings.ids
            auto_create_picking=record.order_id.instance_id.auto_create_return_picking
            record.auto_create_picking=auto_create_picking
    
    @api.one
    @api.depends('order_id')
    def _get_instance(self):
        if self.order_id:
            self.instance_id = self.order_id.instance_id and self.order_id.instance_id.id or False   
                 
    @api.model
    def _default_journal(self):
        inv_type = self._context.get('type', 'out_refund')
        inv_types = inv_type if isinstance(inv_type, list) else [inv_type]
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', 'in', filter(None, map(TYPE2JOURNAL.get, inv_types))),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    @api.one
    def get_error_logs(self):
        amazon_transaction_log_obj=self.env['amazon.transaction.log']
        model_id=amazon_transaction_log_obj.get_model_id('amazon.order.refund.ept')
        logs=amazon_transaction_log_obj.search([('model_id','=',model_id),('res_id','=',self.id)])
        self.log_ids=logs.ids

    order_id=fields.Many2one("amazon.sale.order.ept","Order Ref")
    instance_id = fields.Many2one('amazon.instance.ept',compute='_get_instance',string='Instance',store=True)
    company_id=fields.Many2one("res.company","Company")
    order_total=fields.Float("Order Total",digits=dp.get_precision("Product Price"),related="order_id.amount_total",readonly=True)    
    amazon_refund_line_ids=fields.One2many("amazon.refund.order.lines","amazon_refund_id","Refund Ids")
    state=fields.Selection([('draft','Draft'),('updated_in_amazon','Updated In Amazon'),('validate','Validate'),('cancel','Cancel')],string="State",default='draft')    
    journal_id=fields.Many2one('account.journal', 'Journal', help='You can select here the journal to use for the credit note that will be created. If you leave that field empty, it will use the same journal as the current invoice.',default=_default_journal)
    invoice_id=fields.Many2one("account.invoice","Refund")
    picking_ids=fields.One2many("stock.picking",compute="get_picking")
    auto_create_picking=fields.Boolean("Create Picking",compute="get_picking",store=False)
    date_ept=fields.Date("Date")
    feed_result_id=fields.Many2one("feed.submission.history",string="Feed Id")
    log_ids=fields.One2many('amazon.transaction.log',compute="get_error_logs")

    @api.multi
    def create_flat_file(self):
        file_order_ship = NamedTemporaryFile(delete=False)
        file_order_ship.write("order-id\torder-item-id\tadjustment-reason-code\tcurrency\titem-price-adj\t")
        file_order_ship.write("item-tax-adj\tshipping-price-adj\tshipping-tax-adj\tgift-wrap-price-adj\t")
        file_order_ship.write("gift-wrap-tax-adj\titem-promotion-adj\titem-promotion-id\tship-promotion-adj\t")
        file_order_ship.write("ship-promotion-id\n")
        order_ref=self.order_id.amazon_reference
        currency=self.invoice_id and self.invoice_id.currency_id.name 
        if not currency:
            currency=self.company_id.currency_id.name
        for line in self.amazon_refund_line_ids:
            order_amount=line.order_line_amount * line.product_qty
            order_amount='%.2f'%order_amount
            order_line_tax='%.2f'%line.order_line_tax
            shipping_charge='%.2f'%line.shipping_charge
            shipping_tax='%.2f'%line.shipping_tax
            gift_wrap_charge='%.2f'%line.gift_wrap_charge
            gift_wrap_tax='%.2f'%line.gift_wrap_tax
            if line.item_promotion_adjust>0.0:
                item_promotion_adjust='%.2f'%line.item_promotion_adjust
            else:
                item_promotion_adjust=''
            if line.shipping_promotion_adjust>0.0:
                shipping_promotion_adjust='%.2f'%line.shipping_promotion_adjust
            else:
                shipping_promotion_adjust=''
            file_order_ship.write("%s\t%s\t%s\t%s\t%s\t"%(order_ref,line.amazon_order_line_id.amazon_order_item_id,line.message,currency,order_amount))
            file_order_ship.write("%s\t%s\t%s\t%s\t"%(order_line_tax,shipping_charge,shipping_tax,gift_wrap_charge))
            file_order_ship.write("%s\t%s\t%s\t%s\t"%(gift_wrap_tax,item_promotion_adjust,line.item_promotion_id or '',shipping_promotion_adjust))
            file_order_ship.write("%s\n"%(line.shipping_promotion_id or ''))
        file_order_ship.close()
        fl = file(file_order_ship.name, 'rb')
        data=fl.read()
        return data
    @api.multi
    def update_refund_in_amazon(self):
        self.ensure_one()
        feed_submission_obj=self.env['feed.submission.history']

        instance=self.instance_id
        data=self.create_flat_file()
        mws_obj=Feeds(access_key=str(instance.access_key),secret_key=str(instance.secret_key),account_id=str(instance.merchant_id),region=instance.country_id.amazon_marketplace_code or instance.country_id.code)
        try:
            results=mws_obj.submit_feed(data,'_POST_FLAT_FILE_PAYMENT_ADJUSTMENT_DATA_',marketplaceids=[instance.market_place_id],instance_id=instance.id)
            results=results.parsed
            if results.get('FeedSubmissionInfo',{}).get('FeedSubmissionId',{}).get('value',False):
                last_feed_submission_id=results.get('FeedSubmissionInfo',{}).get('FeedSubmissionId',{}).get('value',False)
                feed=feed_submission_obj.search([('feed_result_id','=',last_feed_submission_id)],order="id desc",limit=1)
                self.write({'feed_result_id':feed.id})
        except Exception,e: 
            raise Warning(str(e))

        file_name = "refund_request_" + time.strftime("%Y_%m_%d_%H%M%S") + '.csv'
        attachment = self.env['ir.attachment'].create({
                                           'name': file_name,
                                           'datas': base64.encodestring(data),
                                           'datas_fname': file_name,
                                           'res_model': 'mail.compose.message',
                                           'type': 'binary'
                                         })
        self.message_post(body=_("<b>Return Created</b>"),attachment_ids=attachment.ids)    
        return True

    @api.multi
    def cancel_refund(self):
        self.write({'state':'cancel'})
        return True
    
    @api.multi
    def reset_to_draft(self):
        self.write({'state':'draft'})
        return True
    @api.multi
    def check_status_in_amazon(self):
        self.ensure_one()
        if not self.feed_result_id:
            return True
        instance=self.order_id.instance_id
        amazon_log_book_obj=self.env['amazon.process.log.book']
        amazon_transaction_obj=self.env['amazon.transaction.log']
        lines=self.amazon_refund_line_ids.ids
        mws_obj=Feeds(access_key=str(instance.access_key),secret_key=str(instance.secret_key),account_id=str(instance.merchant_id),region=instance.country_id.amazon_marketplace_code or instance.country_id.code)        
        try:
            mws_obj.get_feed_submission_result(self.feed_result_id.feed_result_id)
            if hasattr(mws_obj, 'response') and type(mws_obj.response) !=type(None):
                result = str(mws_obj.response.content)
                try:
                    processed=result.split()[-1]
                except:
                    processed='0'
                if processed!=str(len(lines)):
                    job_log_vals = {
                                        'skip_process' :True,
                                        'message':"Return Error",
                                        'application' : 'refund',
                                        'operation_type' : 'export',
                                        'instance_id':instance.id
                                        }
                    job=amazon_log_book_obj.create(job_log_vals)                     
                    if not amazon_transaction_obj.search([('message','=',result),('manually_processed','=',False)]):
                        log_line_vals = {
                                         'model_id' : self.env['amazon.transaction.log'].get_model_id('amazon.order.refund.ept'),
                                         'res_id' : self.id or 0,
                                         'log_type' : 'not_found',
                                         'operation_type':'import',
                                         'user_id' : self.env.uid,
                                         'skip_record' : True,
                                         'amazon_order_reference':self.order_id.name,
                                         'message' :result,
                                         'job_id':job.id
                                         }
                        amazon_transaction_obj.create(log_line_vals)
                    self.write({'state':'cancel'})
                else:
                    if self.auto_create_picking:
                        self.create_return_picking()
                    if self.order_id.instance_id.auto_create_refund:
                        self.create_refund()
                    self.write({'state':'validate'})
        except Exception,e:
            raise Warning(str(e))
        return True

    @api.multi
    def validate(self):
        for record in self:
            for line in record.amazon_refund_line_ids:
                message=False
                if line.total_refund<=0.0:
                    message="Invalid line for %s product"%(line.product_id.name)
                elif line.product_qty<=0.0:
                    message="Invalid Qty for %s product"%(line.product_id.name)
                elif line.product_qty>line.amazon_order_line_id.product_uom_qty:
                    message="Refund Qty is more then order line qty for %s product"%(line.product_id.name)
                elif (line.product_qty+line.qty_canceled) > line.amazon_order_line_id.product_uom_qty:
                    message="Refund & cancel qty are mismatch for %s product"%(line.product_id.name)
                if message:
                    raise Warning(message)
                
            self.update_refund_in_amazon()
            record.write({'state':'updated_in_amazon'})

    @api.multi
    def create_refund(self):
        for record in self:
            account_invoice_line_obj=self.env['account.invoice.line']
            journal_id=record.journal_id and record.journal_id.id
            
            invoice_vals = {
                'name': record.order_id.name or '',
                'origin': record.order_id.name,
                'type': 'out_refund',
                'reference': record.order_id.client_order_ref or record.order_id.name,
                'account_id': record.order_id.partner_id.property_account_receivable.id,
                'partner_id': record.order_id.partner_invoice_id.id,
                'journal_id': journal_id,
                'currency_id': record.order_id.pricelist_id.currency_id.id,
                'comment': record.order_id.note,
                'payment_term': record.order_id.payment_term and record.order_id.payment_term.id or False,
                'fiscal_position': record.order_id.fiscal_position.id or record.order_id.partner_id.property_account_position.id,
                'company_id': record.company_id.id,
                'user_id': record._uid or False,
                'date_invoice':record.date_ept or False,
                'section_id' : record.order_id.section_id and record.order_id.section_id.id
            }
            invoice=self.env['account.invoice'].create(invoice_vals)
            record.write({'invoice_id':invoice.id})
            partner_id=record.order_id.partner_invoice_id.id
            fiscal_position=record.order_id.fiscal_position.id or record.order_id.partner_id.property_account_position.id
            currency_id=record.order_id.pricelist_id.currency_id.id
            for line in record.amazon_refund_line_ids:
                qty=self.env['sale.order.line']._get_line_qty(line.amazon_order_line_id)
                uos_id=self.env['sale.order.line']._get_line_uom(line.amazon_order_line_id)
                result=account_invoice_line_obj.product_id_change(line.product_id.id,uos_id,qty,'','out_refund',partner_id,fiscal_position,line.total_refund,currency_id,record.company_id.id)
                price_unit = round(line.total_refund/ qty,self.env['decimal.precision'].precision_get('Product Price'))
                vals=result.get('value',{})
                tax_ids=line.amazon_order_line_id.tax_id.ids
                vals.update({'invoice_line_tax_id':[(6,0,tax_ids)],'invoice_id':invoice.id,'product_id':line.product_id.id,'price_unit':price_unit,'quantity':qty})
                account_invoice_line_obj.create(vals)           
            return True
    
    @api.multi
    def on_change_lines(self,order_id,amazon_refund_line_ids):
        vals=[]
        if order_id:
            order = self.env['amazon.sale.order.ept'].browse(order_id)    
            for line in order.ept_order_line:
                if line.amazon_product_id:
                    info={
                          'amazon_order_line_id':line.id,
                          'amazon_product_id':line.amazon_product_id.id,
                          'product_id':line.amazon_product_id.product_id.id ,
                          'product_qty':line.product_uom_qty,
                          'price_subtotal':line.price_subtotal,
                          'order_line_amount':line.price_unit,
                          'order_line_tax':line.order_line_tax,
                          'item_promotion_adjust':line.promotion_discount,
                          'shipping_charge':line.shipping_charge_ept,
                          'shipping_tax':line.shipping_charge_tax,
                          'gift_wrap_charge':line.gift_wrapper_charge,
                          'gift_wrap_tax':line.gift_wrapper_tax,
                          'message':'CustomerReturn'
                          }
                    vals.append(info)
            return {'value': {'amazon_refund_line_ids':vals,'company_id':order.warehouse_id.company_id.id }}
        return {}