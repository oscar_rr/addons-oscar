# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class DelvieryData(models.TransientModel):
    _name = 'delivery.data'


    delivered_to = fields.Char(_('Delivered to'))
    delivery_observations = fields.Text(_('Delivery Observations'))

    @api.multi
    def set_data(self):
        so_obj = self.env['service.order']
        if 'active_id' in self._context:
            active_id = self._context.get('active_id')
            so_id = so_obj.browse(active_id)
            if so_id:
                so_id.write({
                    'delivered_to': self.delivered_to,
                    'delivery_observations': self.delivery_observations,
                })
                return so_id.print_delivery()
