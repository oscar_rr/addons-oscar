# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class AssignCreditNote(models.TransientModel):
    _name = 'assign.credit.note'

    name = fields.Many2one('account.invoice', _('Credit Note'))

    @api.multi
    def assign_cred_note(self):
        if self.name:
            class_obj = self.env['service.order']
            act_id_context = class_obj.browse(self._context.get('active_id'))
            for variable in act_id_context:
                variable['credit_note'] = self.name
                variable['status'] = 'authorized_sd'
                variable['stock_validate_compute'] = 3
