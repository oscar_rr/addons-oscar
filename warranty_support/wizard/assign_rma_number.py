# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class AssignRma(models.TransientModel):
    _name = 'assign.rma'

    rma_number = fields.Char(_('RMA Number'))
    delivery_number = fields.Char(_('Delivery number'))

    @api.multi
    def assign_rma_number(self):
        if self.rma_number:
            do_obj = self.env['default.output']
            do_obj.send_to_supplier(self.rma_number, self.delivery_number)
