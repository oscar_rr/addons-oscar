# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class AssignTechnician(models.TransientModel):
    _name = 'assign.technician'

    def get_technicians(self):
        model_data = self.env['ir.model.data']
        object_ref = model_data.get_object_reference('warranty_support', 'group_technician')
        group_obj = self.env['res.groups']
        data = group_obj.search([('id', '=', object_ref[1])])
        users = data.users
        return [('id', 'in', users.ids)]

    name = fields.Many2one(
        'res.users',
        _('Technician'),
        domain=get_technicians
    )

    @api.multi
    def assign_tech(self):
        if self.name:
            class_obj = self.env['service.order']
            act_id_context = class_obj.browse(self._context.get('active_id'))
            for variable in act_id_context:
                variable['technician'] = self.name
                variable['status'] = 'assigned'
