# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class TrafficLines(models.Model):
    _name = 'traffic.lines'

    @api.one
    def get_id(self):
        self.sd_id = self.default_output_id.id

    @api.one
    def get_folio_os(self):
        self.oss_id = self.service_order_id.id

    traffic_id = fields.Many2one(
        'traffic',
        _('Traffic'),
        readonly=True,
    )
    default_output_id = fields.Many2one(
        'default.output',
        _('Default Output'),
        readonly=True,
    )
    sd_id = fields.Integer(
        compute=get_id,
        readonly=True,
    )
    oss_id = fields.Integer(
        compute=get_folio_os,
        readonly=True,
    )
    service_order_id = fields.Many2one(
        related='default_output_id.related_service_order',
        readonly=True,
    )
    product_id = fields.Many2one(
        related='service_order_id.product',
        readonly=True,
    )
    product_serie = fields.Char(
        related='default_output_id.product_serie',
        readonly=True
    )
    default_output_status = fields.Selection(
        related='default_output_id.status',
        readonly=True,
    )
    rma_number = fields.Char(
        _('Supplier RMA'),
        size=64,
        related='default_output_id.rma_number'
    )

    @api.multi
    def get_selected_lines(self, traffic):
        from time import gmtime, strftime
        traffic_obj = self.env['traffic'].browse(traffic)
        for line in self:
            line.default_output_id.write({
                'status': 'in_corporative'
            })
        search_domain = [('traffic_id', '=', traffic)]
        if self:
            search_domain.append(('id', 'not in', self.ids))

        rejected_ids = self.search(search_domain)
        for line in rejected_ids:
            line.default_output_id.write({
                'status': 'rejected_traffic'
            })
        traffic_obj.write({
            'received_user': self.env.user.id,
            'received_date': strftime('%Y-%m-%d %H:%M:%S', gmtime())
        })
        return{
            'received_user': self.env.user.id,
            'received_date': strftime('%Y-%m-%d %H:%M:%S', gmtime())
        }
