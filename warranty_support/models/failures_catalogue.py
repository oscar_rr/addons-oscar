# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class FailuresCatalogue(models.Model):
    _name = 'failures.catalogue'
    _inherit = ['mail.thread']

    name = fields.Char(_('Failure'), required=True)
    description = fields.Text(_('Description'))
    create_date = fields.Date(_('Creation Date'),
                              default=lambda self: fields.datetime.now(),
                              readonly=True)
    line = fields.Many2one('product.category',
                           domain="[('types.type', '=', 'Línea')]",
                           string=_("Line"))
