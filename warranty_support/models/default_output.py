# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, exceptions, _
import datetime


class DefaultOutput(models.Model):
    _name = 'default.output'
    _rec_name = 'name'
    _inherit = ['mail.thread']

    def get_product(self):
        so_obj = self.env['service.order']
        data = so_obj.browse(self._context.get('active_id'))
        return data.product.id

    def get_serie(self):
        so_obj = self.env['service.order']
        data = so_obj.browse(self._context.get('active_id'))
        return data.product_serie

    def get_account_center(self):
        branch_df = self.env['internal.warranty.support.config'].search(
            [],
            limit=1,
        ).branch_df.id
        return branch_df in self.env.user.centro_costo_id.ids

    def _search_name(self, name, args=None, operator='=', limit=1000):
        recs = self.search([])
        branch_df = self.env['internal.warranty.support.config'].search(
            [],
            limit=1,
        ).branch_df.id
        if branch_df in self.env.user.centro_costo_id.ids:
            return [('id', 'in', recs.ids)]
        else:
            return [('status', 'in', ['to_send', 'rejected_traffic'])]

    name = fields.Char(_('Default Output'), readonly=True)
    related_service_order = fields.Many2one(
        'service.order',
        _('Service Order'),
        readonly=True,
        default=lambda self: self._context.get('active_id')
    )
    related_wh_move = fields.Many2one(
        'document.inventory',
        _('Warehouse Move'),
        readonly=True
    )
    related_picking_id = fields.Many2one(
        'stock.picking',
        _('Picking Move'),
        readonly=True
    )

    product = fields.Many2one(
        'product.product',
        _('Product'),
        readonly=True,
        default=get_product
    )
    product_serie = fields.Char(_('Serie'), readonly=True, default=get_serie)
    date = fields.Date(_('Date'), default=lambda self: fields.datetime.now(),
                       readonly=True)
    supplier = fields.Many2one('res.partner', _('Supplier'))
    rma_number = fields.Char(_('Supplier RMA'), size=64)
    delivery_number = fields.Char(_('Delivery Number'))
    status = fields.Selection([
        ('to_send', _('To send')),
        ('to_corporative', _('Sent to corporative')),
        ('in_corporative', _('Received in corporative')),
        ('rejected_traffic', _('Rejected Traffic')),
        ('to_provider', _('Sent to provider')),
        ('delivered', _('Delivered')),
        ('cancelled', _('Cancelled'))
    ], default='to_send',
        index=True,
        track_visibility='always')

    is_sucursal_df = fields.Boolean(
        compute=get_account_center,
        search=_search_name,
    )

    credit_note_payment = fields.Char(_('Credit Note Payment'))
    product_payment = fields.Char(_('Product Payment'))

    observations = fields.Text(_('Observations'))

    @api.multi
    def cancel_sd(self):
        if self.related_picking_id.state == 'cancel':
            wizard_form = self.env.ref(
                'warranty_support.cancel_sd_form_view',
                False
            )

            view_id = self.env['cancel.sd']

            return {
                'name': 'Cancel SD',
                'type': 'ir.actions.act_window',
                'res_model': 'cancel.sd',
                'view_id': wizard_form.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new'
            }
        else:
            raise exceptions.Warning(_('The picking move is not cancelled yet'))

    @api.multi
    def supplier_payment_received(self):
        if self.credit_note_payment or self.product_payment:
            self.status = 'delivered'
        else:
            raise exceptions.Warning(_("You haven't set a payment type yet"))

    @api.multi
    def create_doc_inventory(self):
        conf_obj = self.env['internal.warranty.support.config']
        data = conf_obj.search([], limit=1)
        product = [(0, 0, {'product_id': self.product.id})]

        view_id = self.env['document.inventory']

        vals = {
            'lines_ids': product,
            'check_sd': True,
            'movement_classify': str(data.type_document.movement_classify)
        }

        # We send sd_id in the context to use in the new wizard
        new = view_id.with_context(sd_id=self.id).create(vals)
        if new.id:
            new.picking_id.action_assign()
            if len(self.related_service_order) > 0:
                # When transfering, writes the following values in to it's OS
                so_data = self.env['service.order'].\
                    browse(self._context.get('active_id'))
                so_data.write(
                    {
                        'default_output': self.id,
                        'supplier': self.supplier.id,
                        'status': 'ready_sd'
                    }
                )
            else:
                raise exceptions.Warning(_('You cannot create a SD without a \
            linked Service Order information'))
            return {
                'name': 'Document Inventory',
                'type': 'ir.actions.act_window',
                'res_model': 'document.inventory',
                'res_id': new.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new'
            }
        else:
            raise exceptions.Warning(_("Couldn't create the movement"))

    @api.multi
    def send_to_traffic(self):
        for record in self:
            if record.status not in ['to_send']:
                raise exceptions.Warning(
                    _("""
                      The DO must be in "to sent" status,
                      Please verify and uncheck those that are already sent.
                    """))
        self.write({'status': 'to_corporative'})
        traffic_obj = self.env['traffic']
        lines = []
        for line in self:
            lines.append([0, 0, {
                'default_output_id': line.id
            }])
        res_id = traffic_obj.create({
            'traffic_lines': lines
        })
        return {
            'name': 'Products warning',
            'type': 'ir.actions.act_window',
            'res_model': 'traffic',
            'res_id': res_id.id,
            'view_id': self.env.ref('warranty_support.traffic_form', False).id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'self'
        }

    @api.multi
    def send_to_supplier(self, rma='', delivery_number=''):
        rma = rma.strip()
        delivery_number = delivery_number.strip()
        self = self.browse(self._context.get('ids'))
        self.write(
            {
                'status': 'to_provider',
                'rma_number': rma,
                'delivery_number': delivery_number
            }
        )

    @api.multi
    def rma_number_wizard(self):
        for record in self:
            if record.status not in ['in_corporative']:
                raise exceptions.Warning(
                    _("""
                      The DO must be in "in corporative" status,
                      Please verify again.
                    """))
        return {
            'name': 'Assign RMA Number',
            'type': 'ir.actions.act_window',
            'res_model': 'assign.rma',
            # 'res_id': res_id.id,
            'view_id': self.env.ref('warranty_support.assign_rma_number_form_view', False).id,
            'view_type': 'form',
            'view_mode': 'form',
            'context': {'ids': self.ids},
            'target': 'new'
        }

    @api.one
    def write(self, values):
        if 'rma_number' in values:
            values['status'] = 'to_provider'
        return super(DefaultOutput, self).write(values)
