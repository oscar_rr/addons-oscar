# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, exceptions, _


class CreditNoteRequest(models.Model):
    _name = "credit.note.request"
    _inherit = ['mail.thread']


    name = fields.Char(readonly=True, default=_('Draft'))
    service_order_id = fields.Many2one('service.order', _('Service Order'), track_visibility="onchange",
                                       required=True)
    customer = fields.Many2one(related="service_order_id.customer",
                               readonly=True)
    product_id = fields.Many2one(related="service_order_id.product",
                                 readonly=True)
    product_qty = fields.Integer(_('Product Quantity'), default=1)
    credit_note_id = fields.Many2one('account.invoice', _('Credit Note'), track_visibility="onchange")
    invoice_id = fields.Many2one(related="service_order_id.invoice",
                                 readonly=True,
                                 string="Invoice")
    wh_move_id = fields.Many2one(related="service_order_id.internal_wh_move",
                                 readonly=True,
                                 string="Warehouse move")
    actual_price = fields.Float(related="product_id.product_tmpl_id.list_price",
                                   string="Actual Unit Price")
    actual_price_currency = fields.Many2one('res.currency', 'Currency', readonly=True,
                                            compute="compute_product_price_currency")
    document_amount = fields.Float(_('Document product unit price'), readonly=True,
                                   compute="compute_invoice_id_currency")
    document_currency = fields.Many2one('res.currency', 'Document_currency', readonly=True,
                                        compute="compute_invoice_id_currency")
    credit_note_amount = fields.Float(_('NC Amount'), track_visibility="onchange")
    credit_note_amount_currency = fields.Many2one('res.currency', 'NC Currency', readonly=True,
                                                  compute="compute_product_price_currency")
    deprecation = fields.Float(_('Deprecation (%)'), track_visibility="onchange")
    document_deprecated_amount = fields.Float(_('Unit price deprecated amount'), readonly=True,
                                              compute="compute_deprecated_amounts")
    document_deprecated_amount_currency = fields.Many2one('res.currency', 'DD amount cur', readonly=True,
                                                          compute="compute_invoice_id_currency")
    # credit_note_deprecated_amount = fields.Float(_('Credit Note deprecated amount'), readonly=True,
    #                                              compute="compute_deprecated_amounts")
    # credit_note_deprecated_amount_cur = fields.Many2one('res.currency','CN deprecated amount currency', readonly=True,
    #                                                     compute="compute_product_price_currency")
    product_serie = fields.Char(_('Product serie'), related="service_order_id.product_serie",
                                readonly=True)
    total_amount = fields.Float(_('Total amount'), compute="compute_total_amount")
    total_amount_currency = fields.Many2one('res.currency', 'total amount currency', readonly=True,
                                    compute="compute_product_price_currency")
    status = fields.Selection([
        ('draft', _('Draft')),
        ('waiting', _('Waiting')),
        ('done', _('Done')),
        ('cancel', _('Cancelled'))
    ], default='draft', string="Status", track_visibility="onchange")
    sub_total = fields.Float(_('Sub total'), readonly=True, compute="compute_subtotal")
    sub_total_currency = fields.Many2one('res.currency', 'subtotal currency', readonly=True,
                                         compute="compute_product_price_currency")
    iva = fields.Float(_('Iva'), readonly=True, compute="compute_iva_and_total")
    iva_currency = fields.Many2one('res.currency', 'iva currency', readonly=True,
                                   compute="compute_product_price_currency")
    total = fields.Float(_('Total'), readonly=True,
                         compute="compute_iva_and_total")
    total_currency = fields.Many2one('res.currency', 'total currency', readonly=True,
                                    compute="compute_product_price_currency")

    @api.one
    def set_draft(self):
        self.write({'status': 'draft'})

    @api.one
    def set_waiting(self):
        self.name = \
            self.env['ir.sequence'].next_by_code('credit.note.request.folio')
        self.write({'status': 'waiting'})

    @api.one
    def set_done(self):
        if self.credit_note_id:
            self.write({'status': 'done'})
        else:
            raise exceptions.Warning(_('You need to set a credit note first'))

    @api.one
    def set_cancelled(self):
        self.write({'status': 'cancel'})


    @api.onchange('product_qty')
    def onchange_product_qty(self):
        if self.invoice_id:
            if self.product_id:
                for line in self.invoice_id.invoice_line:
                    if line.product_id.id == self.product_id.id:
                        if self.product_qty > line.quantity:
                            self.product_qty = line.quantity
                            res = {}
                            msg = _("""
                                The quantity is bigger than the product quantity invoiced
                                The current quantity invoiced is %.2f"""%line.quantity)
                            warning = {
                                'title': _("Warning!"),
                                'message': msg
                            }
                            res = {'warning': warning}
                            return res

    @api.depends('invoice_id','wh_move_id','product_qty')
    def compute_invoice_id_currency(self):
        if self.invoice_id:
            if self.product_id:
                for line in self.invoice_id.invoice_line:
                    if line.product_id.id == self.product_id.id:
                        product_doc_unit_price = line.price_unit
                        self.document_amount = product_doc_unit_price
            # if self.actual_price < self.document_amount:
            #     self.credit_note_amount = self.actual_price
            # elif self.actual_price > self.document_amount:
            #     self.credit_note_amount = self.document_amount
            # elif self.actual_price == self.document_amount:
            #     self.credit_note_amount = self.actual_price
            self.credit_note_amount = self.document_amount
            document_currency = self.invoice_id.currency_id.id
            self.document_currency = document_currency
            self.document_deprecated_amount_currency = document_currency
        elif self.wh_move_id:
            self.document_amount = self.actual_price
            # self.document_amount = self.wh_move_id.total
            # if self.product_id.product_tmpl_id.list_price < self.wh_move_id.total:
            #     self.credit_note_amount = self.product_id.product_tmpl_id.list_price
            # elif self.product_id.product_tmpl_id.list_price > self.wh_move_id.total:
            self.credit_note_amount = self.document_amount
            document_currency = self.product_id.product_tmpl_id.list_price_currency.id
            self.document_currency = document_currency
            self.document_deprecated_amount_currency = document_currency

    @api.depends('actual_price')
    def compute_product_price_currency(self):
        product_currency = self.product_id.product_tmpl_id.\
            list_price_currency.id
        document_currency = self.invoice_id.currency_id.id
        self.actual_price_currency = product_currency
        self.credit_note_amount_currency = document_currency
        self.credit_note_deprecated_amount_cur = document_currency
        self.total_amount_currency = document_currency
        self.sub_total_currency = document_currency
        self.iva_currency = document_currency
        self.total_currency = document_currency

    @api.depends('credit_note_amount','deprecation')
    def compute_deprecated_amounts(self):
        # deprecation_nc = (self.credit_note_amount * self.deprecation) / 100
        deprecation_document = (self.credit_note_amount * self.deprecation) / 100
        # self.credit_note_deprecated_amount = self.credit_note_amount - deprecation_nc
        self.document_deprecated_amount = self.credit_note_amount - deprecation_document

    @api.depends('document_deprecated_amount','product_qty')
    def compute_total_amount(self):
        self.total_amount = self.document_deprecated_amount * self.product_qty

    @api.depends('total_amount')
    def compute_subtotal(self):
        self.sub_total = self.total_amount

    @api.depends('sub_total')
    def compute_iva_and_total(self):
        if self.sub_total > 0:
            iva_amount = 0.0
            if self.env.user.company_id.tax_provision_customer:
                iva_amount = self.env.user.company_id.tax_provision_customer.amount
            iva = self.sub_total * iva_amount
            self.iva = iva
            self.total = self.sub_total + self.iva
