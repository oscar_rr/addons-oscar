# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _


class WarrantySupportConfig(models.Model):
    _name = 'warranty.support.config'
    _inherit = 'res.config.settings'

    branch_df = fields.Many2one(
        'account.cost.center',
        _('Branch DF'),
        required=True
    )

    type_document = fields.Many2one(
        'document.inventory.type',
        _('Default Output name'),
        required=True
    )

    warehouse_id = fields.Many2one(
        'stock.warehouse',
        _('Origin Warehouse'),
        required=True
    )

    warehouse_dest_id = fields.Many2one(
        'stock.warehouse',
        _('Destination Warehouse'),
        required=True
    )

    # interwarehouse_doc_type_id = fields.Many2one(
    #     'document.inventory.type',
    #     _('Interwarehouse name'),
    #     required=True
    # )
    #
    # intersucursal_doc_type_id = fields.Many2one(
    #     'document.inventory.type',
    #     _('Intersucursal name'),
    #     required=True
    # )

    ed_id = fields.Many2one(
        'document.inventory.type',
        _('ED'),
        required=True
    )

    ed_origin_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Origin warehouse'),
        required=True
    )

    ed_dest_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Destination warehouse'),
        required=True
    )
    ed_origin_loc_id = fields.Many2one(
        'stock.location',
        _('Origin location'),
        required=True
    )

    ed_dest_loc_id = fields.Many2one(
        'stock.location',
        _('Destination location'),
        required=True
    )

    sai_id = fields.Many2one(
        'document.inventory.type',
        _('SAI'),
        required=True
    )

    sai_origin_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Origin warehouse'),
        required=True
    )

    sai_dest_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Destination warehouse'),
        required=True
    )
    sai_origin_loc_id = fields.Many2one(
        'stock.location',
        _('Origin location'),
        required=True
    )

    sai_dest_loc_id = fields.Many2one(
        'stock.location',
        _('Destination location'),
        required=True
    )

    eai_id = fields.Many2one(
        'document.inventory.type',
        _('EAI'),
        required=True
    )

    eai_origin_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Origin warehouse'),
        required=True
    )

    eai_dest_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Destination warehouse'),
        required=True
    )
    eai_origin_loc_id = fields.Many2one(
        'stock.location',
        _('Origin location'),
        required=True
    )

    eai_dest_loc_id = fields.Many2one(
        'stock.location',
        _('Destination location'),
        required=True
    )

    dga_id = fields.Many2one(
        'document.inventory.type',
        _('DGA'),
        required=True
    )

    dga_origin_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Origin warehouse'),
        required=True
    )

    dga_dest_wh_id = fields.Many2one(
        'stock.warehouse',
        _('Destination warehouse'),
        required=True
    )
    dga_origin_loc_id = fields.Many2one(
        'stock.location',
        _('Origin location'),
        required=True
    )

    dga_dest_loc_id = fields.Many2one(
        'stock.location',
        _('Destination location'),
        required=True
    )

    @api.onchange('ed_id')
    def onchange_ed_id(self):
        if self.ed_id or self.sai_id or self.eai_id or self.dga_id:
            settings_obj = self.env['internal.warranty.support.config'].search([],limit=1)
            msg = _("""This Warehouse is already set in another field...
                        you can't have the same warehouses in two or more fields""")
            warning = {
                'title': _('Warning!'),
                'message': msg
            }
            if self.ed_id.id == self.sai_id.id or self.ed_id.id == self.eai_id.id or self.ed_id.id == self.dga_id.id:
                self.ed_id = settings_obj.ed_id.id
                return {'warning': warning}

    @api.onchange('sai_id')
    def onchange_sai_id(self):
        if self.ed_id or self.sai_id or self.eai_id or self.dga_id:
            settings_obj = self.env['internal.warranty.support.config'].search([],limit=1)
            msg = _("""This Warehouse is already set in another field...
                        you can't have the same warehouses in two or more fields""")
            warning = {
                'title': _('Warning!'),
                'message': msg
            }
            if self.sai_id.id == self.ed_id.id or self.sai_id.id == self.eai_id.id or self.sai_id.id == self.dga_id.id:
                self.sai_id = settings_obj.sai_id.id
                return {'warning': warning}

    @api.onchange('eai_id')
    def onchange_eai_id(self):
        if self.ed_id or self.sai_id or self.eai_id or self.dga_id:
            settings_obj = self.env['internal.warranty.support.config'].search([],limit=1)
            msg = _("""This Warehouse is already set in another field...
                        you can't have the same warehouses in two or more fields""")
            warning = {
                'title': _('Warning!'),
                'message': msg
            }
            if self.eai_id.id == self.ed_id.id or self.eai_id.id == self.sai_id.id or self.eai_id.id == self.dga_id.id:
                self.eai_id = settings_obj.eai_id.id
                return {'warning': warning}

    @api.onchange('dga_id')
    def onchange_dga_id(self):
        if self.ed_id or self.sai_id or self.eai_id or self.dga_id:
            settings_obj = self.env['internal.warranty.support.config'].search([],limit=1)
            msg = _("""This Warehouse is already set in another field...
                        you can't have the same warehouses in two or more fields""")
            warning = {
                'title': _('Warning!'),
                'message': msg
            }
            if self.dga_id.id == self.ed_id.id or self.dga_id.id == self.sai_id.id or self.dga_id.id == self.eai_id.id:
                self.dga_id = settings_obj.dga_id.id
                return {'warning': warning}

    @api.model
    def get_default_warranty_values(self, fields):
        obj = self.env['internal.warranty.support.config'].search(
            [],
            limit=1
        )
        return {
            'branch_df': obj.branch_df.id,
            'type_document': obj.type_document.id,
            'warehouse_id': obj.warehouse_id.id,
            'warehouse_dest_id': obj.warehouse_dest_id.id,
            # 'interwarehouse_doc_type_id': obj.interwarehouse_doc_type_id.id,
            # 'intersucursal_doc_type_id': obj.intersucursal_doc_type_id.id,
            'ed_id': obj.ed_id.id,
            'ed_origin_wh_id': obj.ed_origin_wh_id.id,
            'ed_dest_wh_id': obj.ed_dest_wh_id.id,
            'ed_origin_loc_id': obj.ed_origin_loc_id.id,
            'ed_dest_loc_id': obj.ed_dest_loc_id.id,
            'sai_id': obj.sai_id.id,
            'sai_origin_wh_id': obj.sai_origin_wh_id.id,
            'sai_dest_wh_id': obj.sai_dest_wh_id.id,
            'sai_origin_loc_id': obj.sai_origin_loc_id.id,
            'sai_dest_loc_id': obj.sai_dest_loc_id.id,
            'eai_id': obj.eai_id.id,
            'eai_origin_wh_id': obj.eai_origin_wh_id.id,
            'eai_dest_wh_id': obj.eai_dest_wh_id.id,
            'eai_origin_loc_id': obj.eai_origin_loc_id.id,
            'eai_dest_loc_id': obj.eai_dest_loc_id.id,
            'dga_id': obj.dga_id.id,
            'dga_origin_wh_id': obj.dga_origin_wh_id.id,
            'dga_dest_wh_id': obj.dga_dest_wh_id.id,
            'dga_origin_loc_id': obj.dga_origin_loc_id.id,
            'dga_dest_loc_id': obj.dga_dest_loc_id.id
        }

    @api.multi
    def set_warranty_values(self):
        self._cr.execute(
            """UPDATE internal_warranty_support_config
            SET branch_df=%s, type_document=%s, warehouse_id=%s,
            warehouse_dest_id=%s,ed_id=%s,ed_origin_wh_id=%s,
            ed_dest_wh_id=%s,ed_origin_loc_id=%s,ed_dest_loc_id=%s,sai_id=%s
            ,sai_origin_wh_id=%s,sai_dest_wh_id=%s,sai_origin_loc_id=%s,sai_dest_loc_id=%s,
            eai_id=%s,eai_origin_wh_id=%s,eai_dest_wh_id=%s,eai_origin_loc_id=%s,
            eai_dest_loc_id=%s,dga_id=%s,dga_origin_wh_id=%s,dga_dest_wh_id=%s,
            dga_origin_loc_id=%s,dga_dest_loc_id=%s""", (
                self.branch_df.id,
                self.type_document.id,
                self.warehouse_id.id,
                self.warehouse_dest_id.id,
                # self.interwarehouse_doc_type_id.id,
                # self.intersucursal_doc_type_id.id,
                self.ed_id.id,
                self.ed_origin_wh_id.id,
                self.ed_dest_wh_id.id,
                self.ed_origin_loc_id.id,
                self.ed_dest_loc_id.id,
                self.sai_id.id,
                self.sai_origin_wh_id.id,
                self.sai_dest_wh_id.id,
                self.sai_origin_loc_id.id,
                self.sai_dest_loc_id.id,
                self.eai_id.id,
                self.eai_origin_wh_id.id,
                self.eai_dest_wh_id.id,
                self.eai_origin_loc_id.id,
                self.eai_dest_loc_id.id,
                self.dga_id.id,
                self.dga_origin_wh_id.id,
                self.dga_dest_wh_id.id,
                self.dga_origin_loc_id.id,
                self.dga_dest_loc_id.id)
        )

class InternalWarrantySupportConfig(models.Model):
    _name = 'internal.warranty.support.config'

    branch_df = fields.Many2one('account.cost.center', _('Branch DF'))
    type_document = fields.Many2one(
        'document.inventory.type', _('Default Output name'))
    warehouse_id = fields.Many2one('stock.warehouse', _('Origin Warehouse'))
    warehouse_dest_id = fields.Many2one(
        'stock.warehouse', _('Destination Warehouse'))
    # interwarehouse_doc_type_id = fields.Many2one(
    #     'document.inventory.type', _('Interwarehouse name'))
    # intersucursal_doc_type_id = fields.Many2one(
    #     'document.inventory.type', _('Intersucursal name'))
    ed_id = fields.Many2one('document.inventory.type',
                                              _('ED'))
    ed_origin_wh_id = fields.Many2one('stock.warehouse',
                                              _('Origin warehouse'))
    ed_dest_wh_id = fields.Many2one('stock.warehouse',
                                              _('Destination warehouse'))
    ed_origin_loc_id = fields.Many2one('stock.location',
                                              _('Origin location'))
    ed_dest_loc_id = fields.Many2one('stock.location',
                                              _('Destination location'))
    sai_id = fields.Many2one('document.inventory.type',
                                              _('SAI'))
    sai_origin_wh_id = fields.Many2one('stock.warehouse',
                                              _('Origin warehouse'))
    sai_dest_wh_id = fields.Many2one('stock.warehouse',
                                              _('Destination warehouse'))
    sai_origin_loc_id = fields.Many2one('stock.location',
                                              _('Origin location'))
    sai_dest_loc_id = fields.Many2one('stock.location',
                                              _('Destination location'))
    eai_id = fields.Many2one('document.inventory.type',
                                              _('EAI'))
    eai_origin_wh_id = fields.Many2one('stock.warehouse',
                                              _('Origin warehouse'))
    eai_dest_wh_id = fields.Many2one('stock.warehouse',
                                              _('Destination warehouse'))
    eai_origin_loc_id = fields.Many2one('stock.location',
                                              _('Origin location'))
    eai_dest_loc_id = fields.Many2one('stock.location',
                                              _('Destination location'))
    dga_id = fields.Many2one('document.inventory.type',
                                              _('DGA'))
    dga_origin_wh_id = fields.Many2one('stock.warehouse',
                                              _('Origin warehouse'))
    dga_dest_wh_id = fields.Many2one('stock.warehouse',
                                              _('Destination warehouse'))
    dga_origin_loc_id = fields.Many2one('stock.location',
                                              _('Origin location'))
    dga_dest_loc_id = fields.Many2one('stock.location',
                                              _('Destination location'))

    def init(self, cr):
        cr.execute("""
            SELECT * FROM internal_warranty_support_config LIMIT 1
        """)

        if not cr.fetchone():
            cr.execute("""
                INSERT INTO
                internal_warranty_support_config(branch_df,type_document,warehouse_id,
                warehouse_dest_id,ed_id,
                ed_origin_wh_id,ed_dest_wh_id,ed_origin_loc_id,ed_dest_loc_id,sai_id,
                sai_origin_wh_id,sai_dest_wh_id,sai_origin_loc_id,sai_dest_loc_id,eai_id,
                eai_origin_wh_id,eai_dest_wh_id,eai_origin_loc_id,eai_dest_loc_id,dga_id,
                dga_origin_wh_id,dga_dest_wh_id,dga_origin_loc_id,dga_dest_loc_id)
                 VALUES(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,NULL)
            """)
