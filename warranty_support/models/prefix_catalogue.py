# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _


class PrefixCatalogue(models.Model):
    _name = 'prefix.catalogue'
    _inherit = ['mail.thread']

    name = fields.Char(_('Sequence name'))
    prefix = fields.Char(_('Prefix'), required=True)
    description = fields.Char(_('Description'))
    branch_office = fields.Many2one('account.cost.center', _("Branch office"),
                                    required=True)

    @api.model
    def create(self, vals):
        pref_obj = self.env['prefix.catalogue']
        if vals['branch_office']:
            data = pref_obj.search([])
            for record in data:
                if record.branch_office.id == vals['branch_office']:
                    raise exceptions.Warning(
                        """The branch office '%s' is
                        already set up for the prefix '%s'""" %
                        (record.branch_office.name, record.prefix)
                    )
        return super(PrefixCatalogue, self).create(vals)

    @api.multi
    def write(self, vals):
        pref_obj = self.env['prefix.catalogue']
        if vals['branch_office'] != self.branch_office.id:
            data = pref_obj.search([])
            for record in data:
                if record.branch_office.id == vals['branch_office']:
                    raise exceptions.Warning(
                        """The branch office '%s' is
                        already set up for the prefix '%s'""" %
                        (record.branch_office.name, record.prefix)
                    )
        return super(PrefixCatalogue, self).write(vals)
