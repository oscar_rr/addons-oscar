# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, exceptions, _
from lxml import etree
import datetime
import logging
_logger = logging.getLogger(__name__)


class MultiMovements(models.Model):
    _name = 'multi.movements'
    _rec_name = "sequence"
    _inherit = 'mail.thread'


    sequence = fields.Char(_('Folio'), readonly=True, default=_("Draft"),track_visibility='onchange')
    move_type = fields.Many2one('document.inventory.type',_('Type of movement'),
                                track_visibility='onchange')
    move_type_origin_wh = fields.Many2one('stock.warehouse', compute="get_warehouses",
                                          readonly=True, string=_("Origin Warehouse"))
    move_type_dest_wh = fields.Many2one('stock.warehouse', compute="get_warehouses",
                                        readonly=True, string=_("Dest Warehouse"))
    move_type_origin_loc = fields.Many2one('stock.location', compute="get_warehouses",
                                          readonly=True, string=_("Origin Location"))
    move_type_dest_loc = fields.Many2one('stock.location', compute="get_warehouses",
                                        readonly=True, string=_("Dest Location"))
    reference = fields.Char(_('Reference document'),track_visibility='onchange')
    transfer_date = fields.Datetime(_('Transfer date'))
    document_inventory_id = fields.Many2one('document.inventory',_('Transfer document'))
    line_ids = fields.One2many('multi.movements.lines','multi_movements_id')
    state = fields.Selection([
        ('draft', _('Draft')),
        ('generated', _('Generated')),
        ('transfered', _('Transfered')),
        ('cancelled', _('Cancelled'))
    ], default='draft',
        index=True,
        track_visibility='onchange')

    @api.onchange('move_type')
    def onchange_move_type(self):
        if self.line_ids:
            self.line_ids = False

    @api.one
    def cancel_document(self):
        if self.document_inventory_id:
            if self.document_inventory_id.state not in 'canceled':
                raise exceptions.Warning(_("Transfer document is not cancelled yet"))
            else:
                self.state = 'cancelled'
        else:
            self.state = 'cancelled'

    @api.depends('move_type')
    def get_warehouses(self):
        if self.move_type:
            warehouse_origin_id = False
            warehouse_dest_id = False
            location_origin_id = False
            location_dest_id = False
            settings_obj = self.env['internal.warranty.support.config']
            settings_id = settings_obj.search([],limit=1)
            if settings_id.ed_id.id == self.move_type.id:
                warehouse_origin_id = settings_id.ed_origin_wh_id.id
                warehouse_dest_id = settings_id.ed_dest_wh_id.id
                location_origin_id = settings_id.ed_origin_loc_id.id
                location_dest_id = settings_id.ed_dest_loc_id.id
            elif settings_id.sai_id.id == self.move_type.id:
                warehouse_origin_id = settings_id.sai_origin_wh_id.id
                warehouse_dest_id = settings_id.sai_dest_wh_id.id
                location_origin_id = settings_id.sai_origin_loc_id.id
                location_dest_id = settings_id.sai_dest_loc_id.id
            elif settings_id.eai_id.id == self.move_type.id:
                warehouse_origin_id = settings_id.eai_origin_wh_id.id
                warehouse_dest_id = settings_id.eai_dest_wh_id.id
                location_origin_id = settings_id.eai_origin_loc_id.id
                location_dest_id = settings_id.eai_dest_loc_id.id
            elif settings_id.dga_id.id == self.move_type.id:
                warehouse_origin_id = settings_id.dga_origin_wh_id.id
                warehouse_dest_id = settings_id.dga_dest_wh_id.id
                location_origin_id = settings_id.dga_origin_loc_id.id
                location_dest_id = settings_id.dga_dest_loc_id.id
            self.move_type_origin_wh = warehouse_origin_id
            self.move_type_dest_wh = warehouse_dest_id
            self.move_type_origin_loc = location_origin_id
            self.move_type_dest_loc = location_dest_id

    @api.model
    def fields_view_get(self, view_id=None, view_type='form',
                        toolbar=False, submenu=False):
        res = super(MultiMovements, self).fields_view_get(view_id=view_id, view_type=view_type,
                                            toolbar=toolbar, submenu=submenu)
        settings_obj = self.env['internal.warranty.support.config']
        settings = settings_obj.search([],limit=1)
        if not settings.ed_id or not settings.sai_id or not settings.eai_id or not settings.dga_id:
            raise exceptions.Warning(_("""You do not have the type of movements set yet
                                    please go to settings and set them up"""))
        ids = []
        ids.append(settings.ed_id.id)
        ids.append(settings.sai_id.id)
        ids.append(settings.eai_id.id)
        ids.append(settings.dga_id.id)
        if ids:
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//field[@name='move_type']"):
                node.set('domain', "[('id','in',%s)]"%ids)
            res['arch'] = etree.tostring(doc)
            return res
        else:
            return None

    @api.model
    def create(self, vals):
        if 'reference' in vals:
            if vals['reference']:
                vals['reference'] = vals.get('reference').strip()
        vals['state'] = 'generated'
        vals['sequence'] =\
            self.env['ir.sequence'].next_by_code('multiple.transfer.folio')
        return super(MultiMovements, self).create(vals)

    @api.multi
    def create_doc_inventory(self):
        line_ids = []
        for record in self.line_ids:
            line_ids.append((0, 0,
                            {
                                'product_id': record.product_id.id,
                                'quantity': record.product_qty
                            }))
        view_id = self.env['document.inventory']
        vals = {
            'type_document': self.move_type.id,
            'origin': self.reference,
            'check_mult_moves': True,
            'lines_ids': line_ids,
            'movement_classify': str(self.move_type.movement_classify)
        }
        new = view_id.with_context(mult_id=self.move_type.id).create(vals)
        if new.id:
            self.state = 'transfered'
            self.document_inventory_id = new.id
            self.transfer_date = datetime.datetime.now()
            new.picking_id.action_assign()
            return {
                'name': 'Document Inventory',
                'type': 'ir.actions.act_window',
                'res_model': 'document.inventory',
                'res_id': new.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'current'
            }
        else:
            raise exceptions.Warning(_("Couldn't create the movement"))

class MultiMovementsLines(models.Model):
    _name = 'multi.movements.lines'

    name = fields.Char()
    multi_movements_id = fields.Many2one('multi.movements', 'movements_id')
    product_id = fields.Many2one('product.product', _('Product'), track_visibility='onchange')
    product_qty = fields.Integer(_('Quantity'), default=1, track_visibility='onchange')

    @api.onchange('product_id','product_qty')
    def onchange_product_line(self):
        if 'default_product_id' in self._context:
            self.product_id = self._context.get('default_product_id')
            doc_inventory_type_obj = self.env['document.inventory.type']
            move_type = doc_inventory_type_obj.browse(self._context.get('default_move_type_id'))
            if self.product_id:
                if self.product_qty <= 0:
                    raise exceptions.ValidationError(
                    _('Please enter quantities greater than 0'))
                if move_type.type_operation != 'incoming':
                    var = self.validate_available(move_type)
                    if var != '':
                        stock_warning = var
                        qty = stock_warning.get('qty_update')
                        if qty:
                            self.product_qty = qty
                        else:
                            self.product_id = False
                        return {'warning': stock_warning}

    def validate_available(self, move_type):
        if move_type:
            move_type_id = move_type.id
            settings_obj = self.env['internal.warranty.support.config']
            settings_id = settings_obj.search([],limit=1)
            if settings_id.ed_id.id == move_type_id:
                location_id = settings_id.ed_origin_loc_id
            elif settings_id.sai_id.id == move_type_id:
                location_id = settings_id.sai_origin_loc_id
            elif settings_id.eai_id.id == move_type_id:
                location_id = settings_id.eai_origin_loc_id
            elif settings_id.dga_id.id == move_type_id:
                location_id = settings_id.dga_origin_loc_id
        result = ''
        msg = ''
        quantity = self.product_qty
        product = self.env['product.product'].browse(self.product_id.id)
        if self.product_id.type == 'service':
            result = _("The product %s ") % self.product_id.name +\
            _("is a service")
            return result

        if location_id.usage == 'transit':
            result = _("The location is transit type")
            return result

        new_qty = 0
        product_qty_by_loc = []
        product_qty_by_loc = self.product_id.get_product_available_by_location(
            location_id.id)[self.product_id.id]['virtual_available']
        for location, existencias in product_qty_by_loc:
            qty_available = self.product_id.get_product_available_by_location(
                location_id.id)[self.product_id.id]['qty_available']
            real_qty = qty_available[0][1]
            if quantity:
                self._cr.execute("""
                    SELECT SUM(sq.qty) FROM stock_quant sq
                    JOIN product_product pp
                    ON pp.id = sq.product_id
                    JOIN stock_move sm
                    ON sq.reservation_id = sm.id
                    JOIN stock_location sl
                    ON sq.location_id = sl.id
                    WHERE sq.product_id = %s
                    AND sl.id = %s""", (self.product_id.id, location.id))
                if self._cr.rowcount:
                    apartado = self._cr.fetchone()[0]
                    if apartado is None or apartado is False:
                        apartado = 0.0
                        if quantity > real_qty:
                            msg = _("\nPlan to transfer %.2f %s(s)") % \
                                (self.product_qty, self.product_id.uom_id.name) +\
                                _(" but it only has %.2f %s(s)") % \
                                (real_qty - apartado, self.product_id.uom_id.name)\
                                + _(" available in this location!") +\
                                _("\nThe actual stock (without reserves) is %.2f %s (s)") % \
                                (real_qty, self.product_id.uom_id.name)\
                                + _("\nReserves %.2f %s (s)") % \
                                (apartado, self.product_id.uom_id.name)
                    else:
                        real_qty = real_qty - apartado
                        if quantity > real_qty:
                            msg = _("\nPlan to transfer %.2f %s(s)") % \
                                (self.product_qty, self.product_id.uom_id.name) +\
                                _(" but it only has %.2f %s(s)") % \
                                (real_qty - apartado, self.product_id.uom_id.name)\
                                + _(" available in this location!") +\
                                _("\nThe actual stock (without reserves) is %.2f %s (s)") % \
                                (real_qty, self.product_id.uom_id.name)\
                                + _("\nReserves %.2f %s (s)") % \
                                (apartado, self.product_id.uom_id.name)
                else:
                    msg = _("\nPlan to transfer %.2f %s(s)") % \
                        (self.product_qty, self.product_id.uom_id.name) +\
                        _(" but it only has %.2f %s(s)") % \
                        (real_qty, self.product_id.uom_id.name)\
                        + _(" available in this location!") +\
                        _("\nThe actual stock (without reserves) is %.2f %s (s)") % \
                        (real_qty, self.product_id.uom_id.name)
        if msg:
            result += _("Product [%s] has not enough stock!: "%self.product_id.name) + msg + "\n"

        if result:
            if apartado:
                new_qty = real_qty - apartado
            else:
                new_qty = 0
            warning = {
                'title': _('Warning!'),
                'message': result,
                'qty_update': new_qty
            }
            result = warning
        return result
