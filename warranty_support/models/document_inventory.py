# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class DocumentInventoryInherit(models.Model):
    _inherit = 'document.inventory'

    check_sd = fields.Boolean()
    check_mult_moves = fields.Boolean()

    @api.multi
    def generate(self):
        res = super(DocumentInventoryInherit, self).generate()
        if self.check_sd:
            do_data = self.env['default.output'].\
                browse(self._context.get('sd_id'))
            # do_data.write({'related_wh_move': self.id})
            do_data.write(
                {
                    'name': self.number,
                    'related_picking_id': self.picking_id.id
                }
            )
        return res

    @api.model
    def create(self, vals):
        conf_obj = self.env['internal.warranty.support.config']
        data = conf_obj.search([], limit=1)
        res = super(DocumentInventoryInherit, self).create(vals)
        if res['check_sd']:
            res.sd_id = res.id
            res.type_document = data.type_document.id
            res.warehouse_id = data.warehouse_id.id
            res.on_change_warehouse_id()
            res.warehouse_dest_id = data.warehouse_dest_id.id
            res.on_change_warehouse_dest_id()
            res.generate()
        elif res['check_mult_moves']:
            res.mult_id = res.id
            move_type_id = res._context.get('mult_id')
            if data.ed_id.id == move_type_id:
                type_document_id = data.ed_id.id
                warehouse_origin_id = data.ed_origin_wh_id.id
                warehouse_dest_id = data.ed_dest_wh_id.id
                location_origin_id = data.ed_origin_loc_id.id
                location_dest_id = data.ed_dest_loc_id.id
            elif data.sai_id.id == move_type_id:
                type_document_id = data.sai_id.id
                warehouse_origin_id = data.sai_origin_wh_id.id
                warehouse_dest_id = data.sai_dest_wh_id.id
                location_origin_id = data.sai_origin_loc_id.id
                location_dest_id = data.sai_dest_loc_id.id
            elif data.eai_id.id == move_type_id:
                type_document_id = data.eai_id.id
                warehouse_origin_id = data.eai_origin_wh_id.id
                warehouse_dest_id = data.eai_dest_wh_id.id
                location_origin_id = data.eai_origin_loc_id.id
                location_dest_id = data.eai_dest_loc_id.id
            elif data.dga_id.id == move_type_id:
                type_document_id = data.dga_id.id
                warehouse_origin_id = data.dga_origin_wh_id.id
                warehouse_dest_id = data.dga_dest_wh_id.id
                location_origin_id = data.dga_origin_loc_id.id
                location_dest_id = data.dga_dest_loc_id.id
            res.type_document = type_document_id
            res.warehouse_id = warehouse_origin_id
            res.on_change_warehouse_id()
            res.warehouse_dest_id = warehouse_dest_id
            res.on_change_warehouse_dest_id()
            res.location_id = location_origin_id
            res.location_dest_id = location_dest_id
            res.generate()
        return res
