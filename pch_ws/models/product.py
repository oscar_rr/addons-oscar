# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.tools.translate import _
from suds.client import Client

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    pch_quantity = fields.Integer(
        'Cantidad en PCH',
        readonly=True,
        default=0
    )

    synchronize_with_pch = fields.Boolean(
        'Sincronizar con PCH',
        default=False,
        track_visibility='onchange'
    )

    last_update_stock = fields.Datetime(
        'Ultima actualización PCH'
    )

    def get_pch_stock(self, cr, uid, context={}):
        import time
        res_partner = self.pool.get('res.partner')
        pch_id = res_partner.search(
            cr,
            uid,
            [('supplier', '=', True), ('name', 'ilike', 'pch')]
        )
        ids = self.search(
            cr, uid, [('synchronize_with_pch', '=', True)]
        )

        if pch_id:
            pch = res_partner.browse(cr, uid, pch_id[0])
            service = pch.api_service_type
            if 'soap' in service:
                url = pch.api_manage_docs_url
                if url:
                    try:
                        self.client = Client(url)
                    except Exception as e:
                        raise Warning(e)
                else:
                    raise Warning(""" The partner don´t have configured Url of
                                  API service. """)
            else:
                raise Warning('The partner don´t have configured API service.')

        for _id in ids:
            product = self.browse(cr, uid, _id)
            data = {
                'cliente': pch.api_user,
                'llave': pch.api_password,
                'sku': product.clave_fabricante
            }
            try:
                warehouse_list = pch.api_supplier_center_cost_line
                result = self.client.service.ObtenerArticulo(**data)
                encontrado = False
                if result.estatus:
                    for inventory in result.datos.inventario[::-1]:
                        for warehouse in warehouse_list:
                            if inventory['almacen'] == int(warehouse.identification_warehouse):
                                if inventory.existencia != 0:
                                    product.write({
                                        'pch_quantity': int(inventory.existencia * 30 / 100),
                                        'last_update_stock': time.strftime('%Y-%m-%d %H:%M:%S')
                                    })
                                encontrado = True
                                break
                        if encontrado:
                            break

                    if not encontrado:
                        product.write({
                            'pch_quantity': 0,
                            'last_update_stock': time.strftime('%Y-%m-%d %H:%M:%S')
                        })

            except Exception as e:
                raise Warning(e)
