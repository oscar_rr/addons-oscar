{
    'name': 'PCH WS',
    'version': '0.1',
    'author': '',
    'website': '',
    "category": "Sales",
    "depends": ['base', 'product'],
    'data': [
        'views/product_view.xml',
        'data/ir_cron.xml'
    ],
    'demo_xml': [],
    'active': False,
    'installable': True,
    'application': True,
}
